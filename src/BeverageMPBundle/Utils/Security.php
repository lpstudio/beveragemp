<?php

namespace BeverageMPBundle\Utils;

class Security
{
    /**
     * Generates a cryptographically secure random salt
     * @param int $complexity
     * @return string
     */
    public static function generateSalt(int $complexity = 22)
    {
        return bin2hex(openssl_random_pseudo_bytes($complexity));
    }
}