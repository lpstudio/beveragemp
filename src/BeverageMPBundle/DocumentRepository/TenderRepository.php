<?php

namespace BeverageMPBundle\DocumentRepository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class TenderRepository extends DocumentRepository
{
    public function getLatestTender()
    {
        $qb = $this -> createQueryBuilder();
        $qb -> sort('publication','desc');
        return $qb -> getQuery() -> getSingleResult();
    }
}