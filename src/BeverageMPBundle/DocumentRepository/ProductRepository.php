<?php

namespace BeverageMPBundle\DocumentRepository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ProductRepository extends DocumentRepository
{
    /**
     * Returns the last 5 published products
     * @return mixed
     */
    public function getFeaturedProducts()
    {
        $qb = $this -> createQueryBuilder();
        $qb -> sort('date','DESC') -> limit(5);
        return $qb -> getQuery() -> execute();
    }
}