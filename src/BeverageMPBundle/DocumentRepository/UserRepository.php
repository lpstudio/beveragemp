<?php

namespace BeverageMPBundle\DocumentRepository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserRepository extends DocumentRepository implements UserLoaderInterface
{

    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     */
    public function loadUserByUsername($username)
    {
        $q = $this -> createQueryBuilder()
            -> field('email') -> equals((string)$username)
            -> getQuery();
        try {
            $user = $q -> getSingleResult();
        } catch (NoResultException $e) {
            throw new UsernameNotFoundException(sprintf('Can\'t find Username "%s"', $username),0,$e);
        }
        return $user;
    }
}