<?php

namespace BeverageMPBundle\EventListener;

use BeverageMPBundle\Document\Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Router;

class ExceptionEventListener
{
    private $router,$container,$docm,$twig;

    public function __construct(Router $router, Container $container)
    {
        $this -> router = $router;
        $this -> container = $container;
        $this -> docm = $container -> get('doctrine_mongodb') -> getManager();
        $this -> twig = $container -> get('templating');
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event -> getException();
        $response = new Response();

        $ex = $this -> saveError($exception,$event -> getRequest());

        if ($exception instanceof HttpExceptionInterface){
            switch ($exception -> getStatusCode()){
                case 500:
                    $response -> setContent($this -> twig -> render('@Twig/Exception/error500.html.twig'), array(
                        'hashcode' => $ex -> getHashCode()
                    ));
                    break;
                case 404:
                    $response -> setContent($this -> twig -> render('@Twig/Exception/error404.html.twig'));
                    break;
                case 503:
                    $response -> setContent($this -> twig -> render('@Twig/Exception/error503.html.twig'));
                    break;
            }
            $response -> setStatusCode($exception -> getStatusCode());
        } else {
            $response -> setContent($this -> twig -> render('@Twig/Exception/error500.html.twig'), array(
                'hashcode' => $ex -> getHashCode()
            ));
            $response -> setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $event -> setResponse($response);
    }

    private function saveError(\Exception $exception, Request $request)
    {
        $ex = new Exception();
        $ex -> setDate(new \DateTime('now'))
            -> setCode($exception -> getCode())
            -> setMessage($exception -> getMessage())
            -> setFile($exception -> getFile())
            -> setHashCode(bin2hex(openssl_random_pseudo_bytes(250)))
            -> setIp($request -> getClientIp())
            -> setUser($request -> getUser());
        $this -> docm -> persist($ex);
        $this -> docm -> flush();
        return $ex;
    }
}