<?php

namespace BeverageMPBundle\EventListener;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;

/**
 * Class AuthenticationEventListener
 * @package BeverageMPBundle\EventListener
 */
class AuthenticationEventListener extends DefaultAuthenticationSuccessHandler
{
    protected $router, $security, $container, $docm;

    public function __construct(Router $router, AuthorizationChecker $security, Container $container, HttpUtils $httpUtils, array $options = array())
    {
        parent::__construct($httpUtils, $options);
        $this -> router = $router;
        $this -> security = $security;
        $this -> container = $container;
        $this -> docm = $this -> container -> get('doctrine_mongodb') -> getManager();
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $userRepo = $this -> docm -> getRepository('BeverageMPBundle:User');
        $settingRepo = $this -> docm -> getRepository('BeverageMPBundle:Setting');

        //check user
        if ($token -> getUser() != null){
            $user = $userRepo -> find($token -> getUser() -> getId());

            //checking validity
            if (!$user -> isValidated()){
                $url = $this -> router -> generate('beverage_mp_connect', array(
                    'error' => true,
                    'type' => 1
                ));
                header('Location: ' . $url);
                die();
            }

            //is 2FA enabled
            $setting = $settingRepo -> findOneBy(array(
                'user' => $user
            ));
            if ($setting != null && $setting -> isEnable2fa() === true){
                if ($request -> get('2facode') != null){
                    $twoFaCode = $request -> get('2facode');

                    //checking code
                    $secret = $user -> getTwoFASecret();
                    $auth = new \PHPGangsta_GoogleAuthenticator();
                    $valid = $auth -> verifyCode($secret,$twoFaCode);

                    if ($valid === false){
                        $url = $this -> router -> generate('beverage_mp_connect', array(
                            'with2fa' => true,
                            'error' => true,
                            'type' => 2
                        ));
                        header('Location: ' . $url);
                        die();
                    }
                } else {
                    $url = $this -> router -> generate('beverage_mp_connect', array(
                        'with2fa' => true
                    ));
                    header('Location: ' . $url);
                    die();
                }
            }

            //updating last connect date
            $user -> setLastConnect(new \DateTime('now'));
            $this -> docm -> persist($user);
            $this -> docm -> flush();

        }

        return parent::onAuthenticationSuccess($request,$token);
    }
}