<?php

namespace BeverageMPBundle\Controller;

use BeverageMPBundle\Document\Setting;
use Stripe\Customer;
use Stripe\Error\InvalidRequest;
use Stripe\Invoice;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends Controller
{
    /**
     * This method guesses the user's locale by checking the request
     * or checking the IP address origin country.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function localizeAction(Request $request)
    {
        $lang = 'en'; //fallback locale

        //DETECTING LANGUAGE BY REQUEST
        if (!empty($request -> server -> get('HTTP_ACCEPT_LANGUAGE'))) {
            $lang = substr($request -> server -> get('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        } else {
            //DETECTING LANGUAGE BY GEOLOCATION
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => 'ipinfo.io/' . $request -> server -> get('REMOTE_ADDR')
            ));
            $result = json_decode(curl_exec($ch), true);
            $error = curl_error($ch);

            if ($result != null && $error == null){
                $lang = strtolower($result['country']);
            }
        }

        switch ($lang){
            case 'en':
                return $this -> redirectToRoute('beverage_mp_homepage', array(
                    '_locale' => 'en'
                ));
                break;
            default:
                return $this -> redirectToRoute('beverage_mp_homepage', array(
                    '_locale' => 'en'
                ));
                break;
        }
    }

    /**
     * This method renders the page preloader
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function preloadAction()
    {
        return $this -> render('@BeverageMP/Default/preloader.html.twig');
    }

    /**
     * This method renders the homepage.
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repos
        $productRepo = $mongoMan -> getRepository('BeverageMPBundle:Product');
        $configRepo = $mongoMan -> getRepository('BeverageMPBundle:Config');
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');

        //creating query
        $q = $mongoMan -> createQueryBuilder('BeverageMPBundle:News');

        $pagination = $this -> get('knp_paginator') -> paginate(
            $q,
            $request -> get('page',1),
            '1'
        );

        //getting featured products
        $products = $productRepo -> getFeaturedProducts();

        //loading homepage bloc config
        $homepageBlocConfig = $configRepo -> findOneBy(array(
            'parameterName' => 'homepageBlocCode'
        ));

        return $this->render('BeverageMPBundle:Default:index.html.twig', array(
            'pagination' => $pagination,
            'products' => $products,
            'homepageBlocCode' => $homepageBlocConfig,
            'tender' => $tenderRepo -> getLatestTender()
        ));
    }

    /**
     * This method renders the registration page.
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        //NOT WORKING
        if ($this -> get('security.authorization_checker') -> isGranted('IS_AUTHENTICATED_REMEMBERED')){
            return $this -> redirectToRoute('beverage_mp_homepage');
        }

        //TEMP PATCH
        if ($request -> cookies -> has('REMEMBERME')){
            return $this -> redirectToRoute('beverage_mp_homepage');
        }

        //checking if there is an invite link
        if ($request -> get('invite') != null){
            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $businessInviteRepo = $mongoMan -> getRepository('BeverageMPBundle:BusinessInvitation');

            //checking invite link
            $invite = $businessInviteRepo -> find($request -> get('invite'));

            return $this -> render('@BeverageMP/Default/register.html.twig', array(
                'invite' => $invite
            ));
        }

        return $this -> render('@BeverageMP/Default/register.html.twig');
    }

    /**
     * This method renders the connect page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function connectAction(Request $request)
    {
        //NOT WORKING
        if ($this -> get('security.authorization_checker') -> isGranted('IS_AUTHENTICATED_REMEMBERED')){
            return $this -> redirectToRoute('beverage_mp_homepage');
        }

        //TEMP PATCH
        if ($request -> cookies -> has('REMEMBERME')){
            return $this -> redirectToRoute('beverage_mp_homepage');
        }

        $authUtils = $this -> get('security.authentication_utils');

        $error = $authUtils -> getLastAuthenticationError();

        $lastUsername = $authUtils -> getLastUsername();

        return $this -> render('@BeverageMP/Default/connect.html.twig', array(
            'error' => $error,
            'last_username' => $lastUsername
        ));
    }

    /**
     * This method sends a contact message
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function contactAction(Request $request)
    {
        //checking fiels
        if ($request -> get('name') != null && $request -> get('email') != null
        && $request -> get('phone') != null && $request -> get('message') != null){

            $name = $request -> get('name');
            $email = $request -> get('emaail');
            $phone = $request -> get('phone');
            if ($request -> get('company') != null){
                $company = $request -> get('company');
            } else {
                $company = 'N/A';
            }
            $message = $request -> get('message');

            $this -> get('beverage_mp.notificator') -> sendContactEmail($name,$email,$phone,$company,$message);

            return $this -> redirect($request -> headers -> get('referer'));
        } else {
            return $this -> redirectToRoute('beverage_mp_homepage', array(
                'error' => true,
                'type' => 1
            ));
        }
    }

    /**
     * This method renders the password recovery page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recoverPasswordAction()
    {
        return $this -> render('@BeverageMP/Default/recover-password.html.twig');
    }

    /**
     * This method renders the reset password page
     * @param string $resetId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPasswordAction(string $resetId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $passRecovRepo = $mongoMan -> getRepository('BeverageMPBundle:PasswordRecovery');

        //checking password reset id
        $passReset = $passRecovRepo -> find($resetId);
        if ($passReset != null){

            //checking validity
            $now = new \DateTime('now');
            if ($now <= $passReset -> getExpiration()){

                return $this -> render('@BeverageMP/Default/reset-password.html.twig');

            } else {
                return $this -> redirectToRoute('beverage_mp_recover_password', array(
                    'error' => true,
                    'type' => 3
                ));
            }
        } else {
            return $this -> redirectToRoute('beverage_mp_homepage');
        }
    }

    /**
     * This method renders the personal profile page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction()
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');

        Stripe::setApiKey($this -> getParameter('stripe_secret_key'));

        try {
            //creating customer
            $customer = Customer::retrieve($this -> getUser() -> getCustomerId());

            //gathering invoices
            $invoiceCollection = Invoice::all(array(
                'customer' => $customer -> id
            ));

            $invoices = $invoiceCollection -> data;
        } catch (InvalidRequest $e){
            $invoices = null;
        }

        //getting user's business members
        $business = $this -> getUser() -> getBusiness();
        $users = $userRepo -> findBy(array(
            'business' => $business
        ));

        return $this -> render('@BeverageMP/Default/profile.html.twig', array(
            'invoiceCollection' => $invoices,
            'users' => $users
        ));
    }

    /**
     * This method renders the tender creation page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTenderAction()
    {
        return $this -> render('@BeverageMP/Tender/create.html.twig');
    }

    /**
     * This method renders the create product page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createProductAction()
    {
        return $this -> render('@BeverageMP/Product/create.html.twig');
    }

    /**
     * This method renders the settings page.
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function settingsAction()
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $settingRepo = $mongoMan -> getRepository('BeverageMPBundle:Setting');

        //getting user settings
        $setting = $settingRepo -> findOneBy(array(
            'user' => $this -> getUser()
        ));
        if ($setting == null){
            $setting = new Setting();
            $setting -> setUser($this -> getUser());

            $mongoMan -> persist($setting);
            $mongoMan -> flush();
        }

        return $this -> render('@BeverageMP/Default/settings.html.twig', array(
            'setting' => $setting
        ));
    }

    /**
     * This method renders a static page with seamless .html format
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function staticSlugAction(string $slug)
    {
        switch ($slug){
            case 'plans':
                return $this -> render('@BeverageMP/Static/plans.html.twig');
                break;
            case 'pricing':
                return $this -> render('@BeverageMP/Static/pricing.html.twig');
                break;
            case 'about':
                return $this -> render('@BeverageMP/Static/about.html.twig');
                break;
            case 'legals':
                return $this -> render('@BeverageMP/Static/legals.html.twig');
                break;
            case 'terms':
                return $this -> render('@BeverageMP/Static/terms.html.twig');
                break;
            case 'partners':
                return $this -> render('@BeverageMP/Static/partners.html.twig');
                break;
            case 'contact':
                return $this -> render('@BeverageMP/Static/contact.html.twig');
                break;
            default:
                throw $this -> createNotFoundException();
                break;
        }
    }

    /**
     * This method renders the view business page
     * @param string $businessId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewBusinessAction(string $businessId)
    {
        if (!$this -> isGranted('ROLE_FULL_ACCESS_YEARLY')
            && !$this -> isGranted('ROLE_FULL_ACCESS_MONTHLY')
            && !$this -> isGranted('ROLE_ADMIN')){
            return $this -> redirectToRoute('beverage_mp_static_slug', array(
                'format' => 'html',
                'slug' => 'plans'
            ));
        }

        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');

        //checking business existence
        $business = $businessRepo -> find($businessId);
        if ($business == null){
            throw $this -> createNotFoundException();
        }

        return $this -> render('@BeverageMP/User/view-business.html.twig', array(
            'business' => $business
        ));
    }
}
