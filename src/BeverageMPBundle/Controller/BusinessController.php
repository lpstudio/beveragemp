<?php

namespace BeverageMPBundle\Controller;

use BeverageMPBundle\Document\BusinessInvitation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BusinessController extends Controller
{
    /**
     * This method generates a invitation link for an email
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function generateInviteAction(Request $request)
    {
        if ($request -> get('email') != null){
            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
            $businessInviteRepo = $mongoMan -> getRepository('BeverageMPBundle:BusinessInvitation');

            //checking user unexistence
            $user = $userRepo -> findOneBy(array(
                'email' => $request -> get('email')
            ));
            if ($user != null){
                return $this -> redirectToRoute('beverage_mp_personal_profile', array(
                    'error' => true,
                    'section' => 3,
                    'type' => 1
                ));
            }

            //checking invite unexistence
            $invite = $businessInviteRepo -> findOneBy(array(
                'email' => $request -> get('email')
            ));
            if ($invite != null){
                return $this -> redirectToRoute('beverage_mp_personal_profile', array(
                    'error' => true,
                    'section' => 3,
                    'type' => 1
                ));
            }

            //creating invite
            $invite = new BusinessInvitation();
            $invite -> setEmail($request -> get('email'))
                -> setDate(new \DateTime('now'))
                -> setBusiness($this -> getUser() -> getBusiness());
            $mongoMan -> persist($invite);
            $mongoMan -> flush();

            //creating link
            $link = $this -> generateUrl('beverage_mp_register', array(
                'invite' => $invite -> getId()
            ));

            //sending invite link
            $this -> get('beverage_mp.notificator') -> sendCompanyInvite($this -> getUser(), $request -> get('email'), $link);

            return $this -> redirectToRoute('beverage_mp_personal_profile', array(
                'success' => true,
            ));
        } else {
            return $this -> redirectToRoute('beverage_mp_personal_profile', array(
                'error' => true,
                'section' => 3,
                'type' => 2
            ));
        }
    }

    /**
     * @param Request $request
     * @param string $businessId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function contactAction(Request $request, string $businessId)
    {
        if (!$this -> isGranted('ROLE_FULL_ACCESS_YEARLY')
            && !$this -> isGranted('ROLE_FULL_ACCESS_MONTHLY')
            && !$this -> isGranted('ROLE_ADMIN')){
            return $this -> redirectToRoute('beverage_mp_static_slug', array(
                'format' => 'html',
                'slug' => 'plans'
            ));
        }

        //checking fields
        if ($request -> get('message') != null){

            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');

            //checking business
            $business = $businessRepo -> find($businessId);
            if ($business == null){
                throw $this -> createNotFoundException();
            }

            $this -> get('beverage_mp.notificator') -> sendBusinessContact($business -> getPrimaryContact(),$this -> getUser(),$request -> get('message'));

            return $this -> redirectToRoute('beverage_mp_view_business', array(
                'businessId' => $businessId,
                'success' => true
            ));
        } else {
            return $this -> redirectToRoute('beverage_mp_view_business', array(
                'businessId' => $businessId
            ));
        }
    }
}