<?php

namespace BeverageMPBundle\Controller;

use BeverageMPBundle\Document\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * This method lists the products
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repos
        $productRepo = $mongoMan -> getRepository('BeverageMPBundle:Product');

        //getting products
        $products = $productRepo -> findAll();

        return $this -> render('@BeverageMP/Product/list.html.twig', array(
            'products' => $products
        ));
    }

    /**
     * This method creates a product
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        if ($request -> get('title') != null && $request -> get('description') != null
        && $request -> get('variety') != null && $request -> get('origin') != null
        && $request -> get('format') != null && $request -> get('bac') != null
        && $request -> get('price') != null && $request -> get('currency') != null){

            $title = $request -> get('title');
            $description = $request -> get('description');
            $variety = $request -> get('variety');
            $origin = $request -> get('origin');
            $format = $request -> get('format');
            $bac = $request -> get('bac');
            $price = $request -> get('price');
            $currency = $request -> get('currency');

            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //creating product
            $product = new Product();
            $product -> setTitle($title)
                -> setDescription($description)
                -> setUser($this -> getUser())
                -> setAlc($bac)
                -> setDate(new \DateTime('now'))
                -> setFormat($format)
                -> setOrigin($origin)
                -> setPrice($price)
                -> setCurrency($currency)
                -> setVariety($variety)
                -> generateTinyDesc();

            //setting picture
            if ($request -> files -> get('picture') != null){
                $picture = $request -> files -> get('picture');

                //checking errors
                if ($picture -> getError() > 0){
                    return $this -> redirectToRoute('beverage_mp_create_product', array(
                        'error' => true,
                        'type' => $picture -> getError()
                    ));
                }

                //checking if file is valid
                if ($picture -> isValid()){

                    //checking MIME type
                    if ($picture -> getMimeType() != 'image/gif' && $picture -> getMimeType() != 'image/png'
                    && $picture -> getMimeType() != 'image/jpeg'){
                        return $this -> redirectToRoute('beverage_mp_create_product', array(
                            'error' => true,
                            'type' => 4
                        ));
                    }

                    //upload dir
                    if ($this -> container -> getParameter('kernel.environment') == 'dev'){
                        $projectDir = str_replace('/app_dev.php','',$request -> server -> get('SCRIPT_FILENAME'));
                    } else {
                        $projectDir = str_replace('/app.php','',$request -> server -> get('SCRIPT_FILENAME'));
                    }
                    $uploadDir = $projectDir . '/uploads/product/';

                    //renaming profile pic
                    $filename = substr(md5(uniqid(mt_rand(), true)),0,8);
                    $filename = strtolower($filename . '.' . $picture -> guessExtension());

                    //moving profile pic to dir
                    $picture -> move($uploadDir,$filename);

                    //updating product
                    $product -> setPicture($filename);
                } else {
                    return $this -> redirectToRoute('beverage_mp_create_product', array(
                        'error' => true,
                        'type' => 5
                    ));
                }
            }

            $mongoMan -> persist($product);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_create_product', array(
                'success' => true
            ));

        } else {
            return $this -> redirectToRoute('beverage_mp_create_product', array(
                'error' => true,
                'type' => 1
            ));
        }
    }

    /**
     * This method lists the products of an user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userProductsAction(Request $request)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //create query
        $q = $mongoMan -> createQueryBuilder('BeverageMPBundle:Product')
            -> field('user') -> equals($this -> getUser());

        $paginator = $this -> get('knp_paginator');
        $pagination = $paginator -> paginate(
            $q,
            $request -> get('page',1),
            10
        );

        return $this -> render('@BeverageMP/Product/my-products.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This method deletes the product of an user
     * @param string $productId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(string $productId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $productRepo = $mongoMan -> getRepository('BeverageMPBundle:Product');

        //checking product existence
        $product = $productRepo -> find($productId);
        if ($product != null){
            //checking product owner
            if ($product -> getUser() -> getId() == $this -> getUser() -> getId()){
                //delete product
                $mongoMan -> remove($product);
                $mongoMan -> flush();

                return $this -> redirectToRoute('beverage_mp_profile_products', array(
                    'success' => true
                ));
            } else {
                throw $this -> createNotFoundException();
            }
        } else {
            throw $this -> createNotFoundException();
        }
    }

    public function viewAction(string $productId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $productRepo = $mongoMan -> getRepository('BeverageMPBundle:Product');

        //checking product existence
        $product = $productRepo -> find($productId);
        if ($product != null){
            return $this -> render('@BeverageMP/Product/view.html.twig', array(
                'product' => $product
            ));
        } else {
            throw $this -> createNotFoundException();
        }
    }
}