<?php

namespace BeverageMPBundle\Controller;

use function Couchbase\defaultDecoder;
use Stripe\Coupon;
use Stripe\Customer;
use Stripe\Error\InvalidRequest;
use Stripe\Plan;
use Stripe\Stripe;
use Stripe\Subscription;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Acl\Exception\Exception;

class SubscriptionController extends Controller
{
    private $plans = [];

    /**
     * This method loads plans
     * @throws InvalidRequest
     */
    private function getPlans()
    {
        try {
            Stripe::setApiKey($this -> getParameter('stripe_secret_key'));
            $this -> plans['full-access-monthly'] = Plan::retrieve('full-access-monthly');
            $this -> plans['full-access-yearly'] = Plan::retrieve('full-access-yearly');
        } catch (InvalidRequest $exception){
            throw $exception;
        }
    }

    /**
     *
     * @param Request $request
     * @param string $planId
     * @param int $step
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function subscribeAction(Request $request, string $planId, int $step)
    {
        $this -> getPlans();
        Stripe::setApiKey($this -> getParameter('stripe_secret_key'));

        //checking plan ID
        switch ($planId){
            case 'full-access-monthly':

                //checking current role
                if ($this -> isGranted('ROLE_FULL_ACCESS_MONTHLY')){
                    return $this -> redirectToRoute('beverage_mp_static_slug', array(
                        'slug' => 'plans'
                    ));
                }

                switch ($step){
                    //rendering checkout page
                    case 1:
                        return $this -> render('@BeverageMP/Subscription/step-1.html.twig',array(
                            'plan' => $this -> plans[$planId]
                        ));
                        break;
                    case 2:
                        if ($request -> isMethod('POST')){

                            //check if user has customer id
                            if ($this -> getUser() -> getCustomerId() != null){
                                $customer = Customer::retrieve($this -> getUser() -> getCustomerId());
                            } else {
                                $customer = Customer::create(array(
                                    'email' => $this -> getUser() -> getEmail(),
                                    'source' => $request -> get('token')['id']
                                ));

                                //saving customer id
                                $mongoMan = $this -> get('doctrine_mongodb') -> getManager();
                                $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
                                $user = $userRepo -> find($this -> getUser() -> getId());
                                $user -> setCustomerId($customer -> id);
                                $mongoMan -> persist($user);
                                $mongoMan -> flush();
                            }

                            //create subscription
                            $subscription = Subscription::create(array(
                                'customer' => $customer -> id,
                                'items' => [[
                                    'plan' => $this -> plans['full-access-monthly'] -> id
                                ]]
                            ));

                            //return response
                            return new Response($subscription -> id);

                        } else if ($request -> isMethod('GET') && $request -> get('subid') != null) {

                            $this -> get('security.token_storage') -> getToken() -> setAuthenticated(false);

                            $subscription = Subscription::retrieve($request -> get('subid'));

                            return $this -> render('@BeverageMP/Subscription/step-2.html.twig', array(
                                'subscription' => $subscription
                            ));
                        } else {
                            return new Response(null,Response::HTTP_METHOD_NOT_ALLOWED);
                        }
                    default:
                        throw $this -> createNotFoundException();
                        break;
                }
                break;

            case 'full-access-yearly':

                //checking current role
                if ($this -> isGranted('ROLE_FULL_ACCESS_YEARLY')){
                    return $this -> redirectToRoute('beverage_mp_static_slug', array(
                        'slug' => 'plans'
                    ));
                }

                switch ($step){
                    case 1:
                        return $this -> render('@BeverageMP/Subscription/step-1.html.twig',array(
                            'plan' => $this -> plans[$planId]
                        ));
                        break;
                    case 2:
                        if ($request -> isMethod('POST')){

                            //check if user has customer id
                            if ($this -> getUser() -> getCustomerId() != null){
                                $customer = Customer::retrieve($this -> getUser() -> getCustomerId());
                            } else {
                                $customer = Customer::create(array(
                                    'email' => $this -> getUser() -> getEmail(),
                                    'source' => $request -> get('token')['id']
                                ));

                                //saving customer id
                                $mongoMan = $this -> get('doctrine_mongodb') -> getManager();
                                $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
                                $user = $userRepo -> find($this -> getUser() -> getId());
                                $user -> setCustomerId($customer -> id);
                                $mongoMan -> persist($user);
                                $mongoMan -> flush();
                            }

                            //create subscription
                            $subscription = Subscription::create(array(
                                'customer' => $customer -> id,
                                'items' => [[
                                    'plan' => $this -> plans['full-access-yearly'] -> id
                                ]]
                            ));

                            //return response
                            return new Response($subscription -> id);

                        } else if ($request -> isMethod('GET') && $request -> get('subid') != null) {

                            $this -> get('security.token_storage') -> getToken() -> setAuthenticated(false);

                            $subscription = Subscription::retrieve($request->get('subid'));

                            return $this->render('@BeverageMP/Subscription/step-2.html.twig', array(
                                'subscription' => $subscription
                            ));
                        } else {
                            return new Response(null,Response::HTTP_METHOD_NOT_ALLOWED);
                        }
                    default:
                        throw $this -> createNotFoundException();
                        break;
                }
                break;
            default:
                throw $this -> createNotFoundException();
                break;
        }
    }
}