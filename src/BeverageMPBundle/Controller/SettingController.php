<?php

namespace BeverageMPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SettingController extends Controller
{
    /**
     * This method renders the 2FA configuration page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function twoFAAction()
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //creating authenticator
        $authenticator = new \PHPGangsta_GoogleAuthenticator();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        $user = $userRepo -> find($this -> getUser() -> getId());

        if ($user -> getTwoFASecret() != null){
            $secret = $this -> getUser() -> getTwoFASecret();
        } else {
            $secret = $authenticator -> createSecret();
            $user -> setTwoFASecret($secret);
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        $website = 'TheBeverageMP';
        $title = $this -> getUser() -> getFirstName() . ' ' . $this -> getUser() -> getLastName();
        $qrUrl = $authenticator -> getQRCodeGoogleUrl($title,$secret,$website);

        return $this -> render('@BeverageMP/Default/2fa-config.html.twig', array(
            'secret' => $secret,
            'qrUrl' => $qrUrl
        ));
    }

    public function saveAction(Request $request)
    {
        $twoFa = $request -> get('2fa') != null;

        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $settingRepo = $mongoMan -> getRepository('BeverageMPBundle:Setting');

        //getting setting
        $setting = $settingRepo -> findOneBy(array(
            'user' => $this -> getUser()
        ));

        $setting -> setEnable2fa($twoFa);

        //saving
        $mongoMan -> persist($setting);
        $mongoMan -> flush();

        return $this -> redirectToRoute('beverage_mp_bundle_settings', array(
            'success' => true
        ));
    }
}