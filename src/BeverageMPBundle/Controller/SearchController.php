<?php

namespace BeverageMPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
    public function indexAction()
    {
        return $this -> render('@BeverageMP/Default/search.html.twig');
    }
}