<?php

namespace BeverageMPBundle\Controller;

use BeverageMPBundle\Document\Tender;
use BeverageMPBundle\Document\TenderResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TenderController extends Controller
{
    /**
     * List tenders
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //creating query
        $queryBuilder = $mongoMan -> createQueryBuilder('BeverageMPBundle:Tender');

        $paginator = $this -> get('knp_paginator');
        $pagination = $paginator -> paginate(
            $queryBuilder,
            $request -> get('page',1),
            5
        );

        return $this -> render('@BeverageMP/Tender/list.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This method creates a tender
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        if ($request -> get('title') != null && $request -> get('quantity') != null
        && $request -> get('pricemin') != null && $request -> get('pricemax') != null
        && $request -> get('currency') != null && $request -> get('description') != null
        && $request -> get('location') != null){

            $title = $request -> get('title');
            $description = $request -> get('description');
            $quantity = $request -> get('quantity');
            $priceMin = $request -> get('pricemin');
            $priceMax = $request -> get('pricemax');
            $currency = $request -> get('currency');
            $location = $request -> get('location');

            //create manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //creating tender
            $tender = new Tender();
            $tender -> setUser($this -> getUser())
                -> setCurrency($currency)
                -> setLocation($location)
                -> setTitle($title)
                -> setDescription($description)
                -> setPriceMin($priceMin)
                -> setPriceMax($priceMax)
                -> setQuantity($quantity)
                -> setPublication(new \DateTime('now'));

            if ($request -> get('tinydesc') != null){
                $tender -> generateTinyDesc(true,$request -> get('tinydesc'));
            } else {
                $tender -> generateTinyDesc();
            }

            //validating tender
            $validator = $this -> get('validator');
            $errors = $validator -> validate($tender);
            if (count($errors) > 0){
                throw new \Exception((string)$errors);
            }

            //saving tender
            $mongoMan -> persist($tender);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_create_tender',array(
                'success' => true
            ));
        } else {
            return $this -> redirectToRoute('beverage_mp_create_tender', array(
                'error' => true,
                'type' => 1
            ));
        }
    }

    /**
     * This method is used to view a tender.
     * @param string $tenderId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(string $tenderId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');
        $tenderResponseRepo = $mongoMan -> getRepository('BeverageMPBundle:TenderResponse');

        $tender = $tenderRepo -> find($tenderId);
        if ($tender != null){
            if ($tender -> getUser() -> getId() == $this -> getUser() -> getId()){
                //recovering answers
                $answers = $tenderResponseRepo -> findBy(array(
                    'tender' => $tender
                ));

                return $this -> render('@BeverageMP/Tender/view.html.twig', array(
                    'tender' => $tender,
                    'answers' => $answers
                ));
            } else {
                return $this -> render('@BeverageMP/Tender/view.html.twig', array(
                    'tender' => $tender
                ));
            }
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method shows the user's tenders
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userTendersAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');

        $user = $userRepo -> find($this -> getUser() -> getId());

        $queryBuilder = $mongoMan -> createQueryBuilder('BeverageMPBundle:Tender')
            -> field('user') -> equals($user);

        $paginator = $this -> get('knp_paginator');
        $pagination = $paginator -> paginate(
            $queryBuilder,
            $request -> get('page',1),
            5
        );

        return $this -> render('@BeverageMP/Tender/my-tenders.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This method deletes an user's tender
     * @param string $tenderId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(string $tenderId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');

        //checking tender existence
        $tender = $tenderRepo -> find($tenderId);
        if ($tender != null){
            //checking tender owner
            if ($tender -> getUser() -> getId() == $this -> getUser() -> getId()){
                //delete tender
                $mongoMan -> remove($tender);
                $mongoMan -> flush();

                return $this -> redirectToRoute('beverage_mp_profile_tenders', array(
                    'success' => true
                ));
            } else {
                throw $this -> createNotFoundException();
            }
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method allows users to answer calls for tenders.
     * @param Request $request
     * @param string $tenderId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function answerAction(Request $request, string $tenderId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');

        //checking tender existence
        $tender = $tenderRepo -> find($tenderId);
        if ($tenderId != null){

            //checking tender owner
            if ($tender -> getUser() -> getId() == $this -> getUser() -> getId()){
                return $this -> redirectToRoute('beverage_mp_view_tender', array(
                    'tenderId' => $tenderId,
                    'error' => true,
                    'type' => 1
                ));
            }

            //checking tif there is already a response
            $pseudoResponse = $tenderRepo -> findOneBy(
                array('user' => $this -> getUser()),
                array('tender' => $tender)
            );
            if ($pseudoResponse != null){
                return $this -> redirectToRoute('beverage_mp_view_tender', array(
                    'tenderId' => $tenderId,
                    'error' => true,
                    'type' => 3
                ));
            }

            //checking fields
            if ($request -> get('quantity') != null && $request -> get('price') != null
                && $request -> get('message') != null){

                $quantity = $request -> get('quantity');
                $price = $request -> get('price');
                $message = $request -> get('message');

                //creating response
                $tenderResponse = new TenderResponse();
                $tenderResponse -> setUser($this -> getUser())
                    -> setTender($tender)
                    -> setQuantity($quantity)
                    -> setPrice($price)
                    -> setResponse($message)
                    -> setDate(new \DateTime('now'));

                //validating response
                $validator = $this -> get('validator');
                $errors = $validator -> validate($tender);
                if (count($errors) > 0){
                    throw new \Exception((string)$errors);
                }

                //saving tender response
                $mongoMan -> persist($tenderResponse);
                $mongoMan -> flush();

                //sending email
                $this -> get('beverage_mp.notificator') -> sendAnswerNotification($this -> getUser(),$tender);

                return $this -> redirectToRoute('beverage_mp_view_tender', array(
                    'tenderId' => $tenderId,
                    'success' => true
                ));
            } else {
                return $this -> redirectToRoute('beverage_mp_view_tender', array(
                    'tenderId' => $tenderId,
                    'error' => true,
                    'type' => 2
                ));
            }

        } else {
            throw $this -> createNotFoundException();
        }
    }
}