<?php

namespace BeverageMPBundle\Controller;

use BeverageMPBundle\Document\Business;
use BeverageMPBundle\Document\EmailValidation;
use BeverageMPBundle\Document\PasswordRecovery;
use BeverageMPBundle\Document\User;
use BeverageMPBundle\Utils\Security;
use Stripe\Charge;
use Stripe\Error\InvalidRequest;
use Stripe\Invoice;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TCPDF;

class UserController extends Controller
{
    /**
     * This method receives the post parameters from the
     * registration form.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function registerAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');
        $businessInviteRepo = $mongoMan -> getRepository('BeverageMPBundle:BusinessInvitation');

        //checking if there is an invite
        if ($request -> get('invite') != null){

            //checking fields
            if ($request -> get('firstname') != null && $request -> get('lastname') != null
            && $request -> get('email') != null && $request -> get('password') != null
            && $request -> get('type') != null){

                //creating vars
                $firstName = $request -> get('firstname');
                $lastName = $request -> get('lastname');
                $email = $request -> get('email');
                $password = $request -> get('password');
                $type = $request -> get('type');
                $inviteId = $request -> get('invite');

                //checking invite existence
                $invite = $businessInviteRepo -> find($inviteId);
                if ($invite != null){
                    //checking if emails are matching
                    if ($invite -> getEmail() === $email){

                        //checking is user is unique
                        $user = $userRepo -> findOneBy(array(
                            'email' => $email
                        ));
                        if ($user != null){
                            return $this -> redirectToRoute('beverage_mp_register', array(
                                'error' => true,
                                'type' => 2
                            ));
                        }

                        //creating user
                        $user = new User();
                        $user -> setFirstName($firstName)
                            -> setLastName($lastName)
                            -> setEmail($email)
                            -> setRegistration(new \DateTime('now'))
                            -> setType($type)
                            -> setValidated(false);

                        //hashing password
                        $salt = Security::generateSalt();
                        $password = password_hash($password, PASSWORD_BCRYPT, array('salt' => $salt));
                        $user -> setSalt($salt)
                            -> setPassword($password);

                        //optionnal fields
                        if ($request -> get('function') != null){
                            $user -> setFunction($request -> get('function'));
                        }

                        if ($request -> get('phone') != null){
                            $user -> setPhone($request -> get('phone'));
                        }

                        //setting role
                        if (in_array($type, User::$TYPE_PRODUCER)){
                            $user -> addRole(User::$ROLE_PRODUCER);
                        } elseif (in_array($type, User::$TYPE_USER)){
                            $user -> addRole(User::$ROLE_USER);
                        } else {
                            $user -> addRole(User::$ROLE_USER);
                        }

                        //associating user to company
                        $user -> setBusiness($invite -> getBusiness());

                        //validating user
                        $errors = $this -> get('validator') -> validate($user);
                        if (count($errors) > 0){
                            throw new \Exception($errors);
                        }

                        //saving user
                        $mongoMan -> persist($user);
                        $mongoMan -> flush();

                        //sending validation email
                        $validation = new EmailValidation();
                        $validation -> setUser($user);
                        $mongoMan -> persist($validation);
                        $mongoMan -> flush();
                        $link = 'https://www.thebeveragemarket.com' . $this -> generateUrl('beverage_mp_validate_email', array(
                            'validationId' => $validation -> getId()
                            ));
                        $this -> get('beverage_mp.notificator') -> sendValidationMail($user,$link);

                        return $this -> redirectToRoute('beverage_mp_register', array(
                            'success' => true
                        ));
                    } else {
                        return $this -> redirectToRoute('beverage_mp_register');
                    }
                }
            } else {
                return $this -> redirectToRoute('beverage_mp_register', array(
                    'error' => true,
                    'type' => 1
                ));
            }
        } else {
            //checking fields
            if ($request -> get('firstname') != null && $request -> get('lastname') != null
            && $request -> get('email') != null && $request -> get('password') != null
            && $request -> get('name') != null && $request -> get('companyphone') != null
            && $request -> get('address') != null && $request -> get('city') != null
            && $request -> get('state') != null && $request -> get('zip') != null
            && $request -> get('country') != null && $request -> get('type') != null){

                //creating vars
                $firstName = $request -> get('firstname');
                $lastName = $request -> get('lastname');
                $email = $request -> get('email');
                $password = $request -> get('password');
                $name = $request -> get('name');
                $companyPhone = $request -> get('companyphone');
                $address = $request -> get('address');
                $city = $request -> get('city');
                $state = $request -> get('state');
                $zip = $request -> get('zip');
                $country = $request -> get('country');
                $type = $request -> get('type');

                //checking user unicity
                $user = $userRepo -> findOneBy(array(
                    'email' => $email
                ));
                if ($user != null){
                    return $this -> redirectToRoute('beverage_mp_register', array(
                        'error' => true,
                        'type' => 2
                    ));
                }

                //creating user
                $user = new User();
                $user -> setEmail($email)
                    -> setRegistration(new \DateTime('now'))
                    -> setFirstName($firstName)
                    -> setLastName($lastName)
                    -> setType($type)
                    -> setValidated(false);

                //hashing password
                $salt = Security::generateSalt();
                $password = password_hash($password, PASSWORD_BCRYPT, array('salt' => $salt));
                $user -> setSalt($salt)
                    -> setPassword($password);

                //setting role
                if (in_array($type, User::$TYPE_PRODUCER)){
                    $user -> addRole(User::$ROLE_PRODUCER);
                } elseif (in_array($type, User::$TYPE_USER)){
                    $user -> addRole(User::$ROLE_USER);
                } else {
                    $user -> addRole(User::$ROLE_USER);
                }

                //optional fields
                if ($request -> get('function') != null){
                    $user -> setFunction($request -> get('function'));
                }

                if ($request -> get('phone') != null){
                    $user -> setPhone($request -> get('phone'));
                }

                //saving user
                $mongoMan -> persist($user);
                $mongoMan -> flush();

                //checking company unicity
                $company = $businessRepo -> findOneBy(array(
                    'name' => $name
                ));
                if ($company != null){
                    $mongoMan -> remove($user);
                    $mongoMan -> flush();

                    return $this -> redirectToRoute('beverage_mp_register', array(
                        'error' => true,
                        'type' => 3
                    ));
                }

                //creating company
                $company = new Business();
                $company -> setName($name)
                    -> setPhone($companyPhone)
                    -> setAddress($address)
                    -> setCity($city)
                    -> setState($state)
                    -> setZip($zip)
                    -> setCountry($country)
                    -> setPrimaryContact($user);

                //optionnal fields
                if ($request -> get('companyemail') != null){
                    $company -> setEmail($request -> get('companyemail'));
                }

                if ($request -> get('website') != null){
                    $company -> setWebsite($request -> get('website'));
                }

                if ($request -> get('commercialname') != null){
                    $company -> setCommercialName($request -> get('commercialname'));
                }

                //saving company
                $mongoMan -> persist($company);
                $mongoMan -> flush();

                //updating user business
                $user -> setBusiness($company);

                $mongoMan -> persist($user);
                $mongoMan -> flush();

                //sending validation email
                $validation = new EmailValidation();
                $validation -> setUser($user);
                $mongoMan -> persist($validation);
                $mongoMan -> flush();
                $link = 'https://www.thebeveragemarket.com' . $this -> generateUrl('beverage_mp_validate_email', array(
                        'validationId' => $validation -> getId()
                    ));
                $this -> get('beverage_mp.notificator') -> sendValidationMail($user,$link);

                return $this -> redirectToRoute('beverage_mp_register', array(
                    'success' => true
                ));

            } else {
                return $this -> redirectToRoute('beverage_mp_register', array(
                    'error' => true,
                    'type' => 1
                ));
            }
        }
    }

    /**
     * This method set the email of an user as valid or not.
     * @param string $validationId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function validateAction(string $validationId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $emailValidationRepo = $mongoMan -> getRepository('BeverageMPBundle:EmailValidation');
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking vaidation existence
        $validation = $emailValidationRepo -> find($validationId);
        if ($validation != null){
            $userTemp = $validation -> getUser();

            //verify user existence
            $user = $userRepo -> find($userTemp -> getId());
            if ($user != null){

                //set user as validated
                $user -> setValidated(true);
                $mongoMan -> persist($user);
                $mongoMan -> flush();

                //delete validation
                $mongoMan -> remove($validation);
                $mongoMan -> flush();

                return $this -> redirectToRoute('beverage_mp_connect', array(
                    'validated' => true
                ));
            } else {
                return $this -> redirectToRoute('beverage_mp_connect');
            }
        } else {
            return $this -> redirectToRoute('beverage_mp_connect');
        }
    }

    /**
     * This method should never be executed during the connection
     * process.
     */
    public function connectAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * This method logouts the user by deleting the security token
     * and invalidating the session.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction(Request $request)
    {
        $this -> get('security.token_storage') -> setToken(null);
        $request -> getSession() -> invalidate();
        return $this -> redirectToRoute('beverage_mp_homepage');
    }

    /**
     * This method creates and sends a reset link to the user's email
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function recoverPasswordAction(Request $request)
    {
        if ($request -> get('email') != null){

            $email = $request -> get('email');

            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

            //checking user existence
            $user = $userRepo -> findOneBy(array(
                'email' => $email
            ));
            if ($user != null){

                //creating password reset
                $expDate = new \DateTime('now');
                $expDate -> modify('+1 day');
                $passReset = new PasswordRecovery();
                $passReset -> setUser($user)
                    -> setExpiration($expDate);

                //saving password reset
                $mongoMan -> persist($passReset);
                $mongoMan -> flush();

                //creating reset link
                $link = 'https://www.thebeveragemarket.com' . $this -> generateUrl('beverage_mp_reset_password', array(
                    'resetId' => $passReset -> getId()
                ));

                //sending email
                $this -> get('beverage_mp.notificator') -> sendResetPasswordEmail($user,$link);

                return $this -> redirectToRoute('beverage_mp_recover_password', array(
                    'success' => true
                ));

            } else {
                return $this -> redirectToRoute('beverage_mp_recover_password', array(
                    'error' => true,
                    'type' => 2
                ));
            }
        } else {
            return $this -> redirectToRoute('beverage_mp_recover_password', array(
                'error' => true,
                'type' => 1
            ));
        }
    }

    /**
     * This method resets the user's password
     * @param Request $request
     * @param string $resetId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetPasswordAction(Request $request, string $resetId)
    {
        if ($request -> get('password') != null && $request -> get('passwordconfirm') != null){
            $password = $request -> get('password');
            $passwordConf = $request -> get('passwordconfirm');

            if ($password !== $passwordConf){
                return $this -> redirectToRoute('beverage_mp_reset_password', array(
                    'resetId' => $resetId,
                    'error' => true,
                    'type' => 2
                ));
            }

            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $passRecovRepo = $mongoMan -> getRepository('BeverageMPBundle:PasswordRecovery');
            $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

            //checking password reset id
            $passReset = $passRecovRepo -> find($resetId);
            if ($passReset != null){

                //checking validity
                $now = new \DateTime('now');
                if ($now <= $passReset -> getExpiration()){

                    //check user existence
                    $user = $userRepo -> find($passReset -> getUser() -> getId());
                    if ($user != null){

                        //changing password
                        $salt = Security::generateSalt();
                        $password = password_hash($password, PASSWORD_BCRYPT, array('salt' => $salt));

                        $user -> setPassword($password)
                            -> setSalt($salt);

                        //update user
                        $mongoMan -> persist($user);
                        $mongoMan -> flush();

                        //delete password recovery
                        $mongoMan -> remove($passReset);
                        $mongoMan -> flush();

                        return $this -> redirectToRoute('beverage_mp_reset_password', array(
                            'success' => true
                        ));
                    } else {
                        throw $this -> createNotFoundException();
                    }

                } else {
                    return $this -> redirectToRoute('beverage_mp_recover_password', array(
                        'error' => true,
                        'type' => 3
                    ));
                }
            } else {
                return $this -> redirectToRoute('beverage_mp_homepage');
            }
        } else {
            return $this -> redirectToRoute('beverage_mp_reset_password', array(
                'resetId' => $resetId,
                'error' => true,
                'type' => 1
            ));
        }
    }

    /**
     * This method edits the user's profile
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editProfileAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //getting true user object
        $user = $userRepo -> find($this -> getUser() -> getId());

        /*
         * ===============================================
         *               CHANGE PROFILE PIC
         * ===============================================
         */
        if ($request -> files -> get('profilepic') != null){
            $profilePic = $request -> files -> get('profilepic');

            //checking errors
            if ($profilePic -> getError() > 0){
               $error = $profilePic -> getError();
                return $this -> redirectToRoute('beverage_mp_personal_profile',array(
                    'error' => true,
                    'section' => 1,
                    'type' => $error
                ));
            }

            //checking if file is valid
            if ($profilePic -> isValid()){

                //checking MIME type
                if ($profilePic -> getMimeType() != 'image/gif' && $profilePic -> getMimeType() != 'image/png'
                && $profilePic -> getMimeType() != 'image/jpeg'){
                    return $this -> redirectToRoute('beverage_mp_personal_profile',array(
                        'error' => true,
                        'section' => 1,
                        'type' => 4
                    ));
                }

                //upload dir
                if ($this -> container -> getParameter('kernel.environment') == 'dev'){
                    $projectDir = str_replace('/app_dev.php','',$request -> server -> get('SCRIPT_FILENAME'));
                } else {
                    $projectDir = str_replace('/app.php','',$request -> server -> get('SCRIPT_FILENAME'));
                }
                $uploadDir = $projectDir . '/uploads/profile/';

                //deleting old profile pic if it is not blank.png
                if ($user -> getProfilePic() != 'blank.png'){
                    unlink($uploadDir . $user -> getProfilePic());
                }

                //renaming profile pic
                $filename = substr(md5(uniqid(mt_rand(), true)),0,8);
                $filename = strtolower($filename . '.' . $profilePic -> guessExtension());

                //moving profile pic to dir
                $profilePic -> move($uploadDir,$filename);

                //updating user
                $user -> setProfilePic($filename);
                $mongoMan -> persist($user);
                $mongoMan -> flush();
            } else {
                return $this -> redirectToRoute('beverage_mp_personal_profile',array(
                    'error' => true,
                    'section' => 1,
                    'type' => 5
                ));
            }
        }

        /*
         * ===============================================
         *               CHANGE FIRSTNAME
         * ===============================================
         */
        if ($request -> get('firstname') != null){
            $user -> setFirstName($request -> get('firstname'));
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        /*
         * ===============================================
         *               CHANGE LASTNAME
         * ===============================================
         */
        if ($request -> get('lastname') != null){
            $user -> setLastName($request -> get('lastname'));
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        /*
         * ===============================================
         *               CHANGE ADDRESS
         * ===============================================
         */
        if ($request -> get('address') != null){
            $user -> setAddress($request -> get('address'));
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        /*
         * ===============================================
         *               CHANGE PHONE
         * ===============================================
         */
        if ($request -> get('phone') != null){
            $user -> setPhone($request -> get('phone'));
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        /*
         * ===============================================
         *               CHANGE TYPE
         * ===============================================
         */
        if ($request -> get('type') != null){
            $user -> setType($request -> get('type'));
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        /*
         * ===============================================
         *               CHANGE FAX
         * ===============================================
         */
        if ($request -> get('fax') != null){
            $user -> setFax($request -> get('fax'));
            $user -> resetRoles();
            $mongoMan -> persist($user);
            $mongoMan -> flush();
        }

        /*
         * ===============================================
         *               CHANGE EMAIL
         * ===============================================
         */
        if ($request -> get('email') != null && $request -> get('emailconfirm')){

            if ($request -> get('email') !== $request -> get('emailconfirm')){
                return $this -> redirectToRoute('beverage_mp_personal_profile',array(
                    'error' => true,
                    'section' => 2,
                    'type' => 1
                ));
            }

            //send email confirm
            $emailVerification = new EmailValidation();
            $emailVerification -> setUser($user);

            //save verification
            $mongoMan -> persist($emailVerification);
            $mongoMan -> flush();

            //generating validation link
            $link = 'https://www.thebeveragemarket.com' . $this -> generateUrl('beverage_mp_validate_email', array(
                'validationId' => $emailVerification -> getId()
            ));

            //sending confirmation email
            $this -> get('beverage_mp.notificator') -> sendValidationMail($user,$link);

            //save new mail
            $user -> setEmail($request -> get('email'));
            $user -> setValidated(false);
            $mongoMan -> persist($user);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_personal_profile', array(
                'success' => true,
                'type' => 1
            ));
        }

        return $this -> redirectToRoute('beverage_mp_personal_profile', array(
            'success' => true
        ));
    }

    /**
     * This method renders the directory page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function directoryAction()
    {
        return $this -> render('@BeverageMP/User/directory.html.twig');
    }

    /**
     * This method generates a PDF invoice
     * @param string $invoiceId
     */
    public function downloadInvoiceAction(string $invoiceId)
    {
        Stripe::setApiKey($this -> getParameter('stripe_secret_key'));

        try {
            //getting invoice
            $invoice = Invoice::retrieve($invoiceId);

            //getting charge
            $charge = Charge::retrieve($invoice -> charge);
        } catch (InvalidRequest $exception){
            throw $this -> createNotFoundException();
        }

        //creating pdf
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION,PDF_UNIT,PDF_PAGE_FORMAT,true,'UTF-8',false);

        //set document information
        $pdf -> SetCreator('TBMP Invoice System');
        $pdf -> SetAuthor('The Beverage Marketplace');
        $pdf -> SetTitle($invoice -> number);
        $pdf -> SetSubject(sprintf('Invoice %s',$invoice -> number));

        //removing header
        $pdf -> setPrintHeader(false);

        //set default monospaced font
        $pdf -> SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf -> SetMargins(PDF_MARGIN_LEFT,PDF_MARGIN_TOP,PDF_MARGIN_RIGHT);
        $pdf -> setFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page break
        $pdf -> SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf -> setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set font
        $pdf -> SetFont('dejavusans','',10);

        //add page
        $pdf -> AddPage();

        $pdf -> writeHTML($this -> renderView('@BeverageMP/PDF/invoice.html.twig', array(
            'title' => sprintf('Invoice %s',$invoice -> number),
            'invoice' => $invoice,
            'charge' => $charge
        )));

        $pdf -> lastPage();

        $pdf -> Output($invoice -> number);
    }

    /**
     * This method edits the user's business
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editBusinessAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');

        //loading business
        $business = $this -> getUser() -> getBusiness();

        if ($business -> getPrimaryContact() != $this -> getUser()){
            return $this -> redirectToRoute('beverage_mp_personal_profile', array(
                'error' => true,
                'section' => 2,
                'type' => 1
            ));
        }

        //changing values
        if ($request -> get('commercialname') != null){
            $business -> setCommercialName($request -> get('commercialname'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('phone') != null){
            $business -> setPhone($request -> get('phone'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('website') != null){
            $business -> setWebsite($request -> get('website'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('address') != null){
            $business -> setAddress($request -> get('address'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('city') != null){
            $business -> setCity($request -> get('city'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('state') != null){
            $business -> setState($request -> get('state'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('zip') != null){
            $business -> setZi^p($request -> get('zip'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        if ($request -> get('country') != null){
            $business -> setCountry($request -> get('country'));
            $mongoMan -> persist($business);
            $mongoMan -> flush();
        }

        return $this -> redirectToRoute('beverage_mp_personal_profile', array(
            'success' => true
        ));
    }
}