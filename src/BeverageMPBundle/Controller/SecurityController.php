<?php

namespace BeverageMPBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{
    public function check2FAAction(Request $request)
    {
        $response = new JsonResponse();
        if ($request -> get('code') != null){

            $code = $request -> get('code');

            //getting secret
            $secret = $this -> getUser() -> getTwoFASecret();

            //checking code
            $authenticator = new \PHPGangsta_GoogleAuthenticator();
            $valid = $authenticator -> verifyCode($secret,$code);

            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $settingRepo = $mongoMan -> getRepository('BeverageMPBundle:Setting');

            //loading user settings
            $setting = $settingRepo -> findOneBy(array(
                'user' => $this -> getUser()
            ));

            if ($valid){
                $setting -> setEnable2fa(true);
            } else {
                $setting -> setEnable2fa(false);
            }

            $mongoMan -> persist($setting);
            $mongoMan -> flush();

            $response -> setContent(json_encode(array(
                'valid' => $valid
            )));
            return $response;
        } else {
            $response -> setContent(json_encode(array(
                'error' => 'MISSING_FIELD'
            )));
            $response -> setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            return $response;
        }
    }
}