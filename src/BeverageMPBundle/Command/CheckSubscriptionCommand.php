<?php

namespace BeverageMPBundle\Command;

use BeverageMPBundle\Document\User;
use Stripe\Customer;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckSubscriptionCommand extends ContainerAwareCommand
{
    private $dm, $userRepo;

    protected function configure()
    {
        $this -> setName('user:check:subscription')
            -> setDescription('Scans all users and remove subscription according to Stripe API');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $time = microtime();

        $this -> dm = $this -> getContainer() -> get('doctrine_mongodb') -> getManager();
        $this -> userRepo = $this -> dm -> getRepository('BeverageMPBundle:User');

        $output -> writeln('<info>Checking user subscription with Stripe API</info>');

        Stripe::setApiKey($this -> getContainer() -> getParameter('stripe_secret_key'));

        $users = $this -> userRepo -> findAll();

        foreach ($users as $user){
            if ($user -> getCustomerId() != null && (in_array(User::$ROLE_FULL_ACCESS_MONTHLY, $user -> getRoles()) || in_array(User::$ROLE_FULL_ACCESS_YEARLY, $user -> getRoles()))){
                $customer = Customer::retrieve($user -> getCustomerId());
                $subscriptions = $customer -> subscriptions;

                foreach ($subscriptions -> data as $subscription){
                    if ($subscription -> items -> data -> plan -> id === 'full-access-monthly'){

                    }
                }
            }
        }
    }
}