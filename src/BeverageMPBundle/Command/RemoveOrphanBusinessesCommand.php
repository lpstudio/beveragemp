<?php

namespace BeverageMPBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveOrphanBusinessesCommand extends ContainerAwareCommand
{
    private $docm;

    protected function configure()
    {
        $this -> setName('business:remove-orphans')
            -> setDescription('Deletes businesses without users');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $time = microtime(true);
        $counter = 0;

        $this -> docm = $this -> getContainer() -> get('doctrine_mongodb') -> getManager();

        //getting repos
        $userRepo = $this -> docm -> getRepository('BeverageMPBundle:User');
        $businessRepo = $this -> docm -> getRepository('BeverageMPBundle:Business');

        //getting all businesses
        $businesses = $businessRepo -> findAll();
        foreach ($businesses as $business){
            $users = $userRepo -> findBy(array(
                'business' => $business
            ));
            if ($users == null){
                $this -> docm -> remove($business);
                $this -> docm -> flush();
                $counter++;
            }
        }

        $time = microtime(true) - $time;

        $output -> writeln('<info>Deleted ' . $counter . ' businesses in ' . $time . ' ms</info>');
    }
}