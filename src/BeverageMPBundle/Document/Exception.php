<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\ODM\PHPCR\Mapping\Annotations\Field;

/**
 * Class Exception
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document()
 */
class Exception
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    private $date;

    /**
     * @var int
     *
     * @MongoDB\Field(type="integer")
     */
    private $code;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    private $message;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    private $file;

    /**
     * @var int
     *
     * @MongoDB\Field(type="integer")
     */
    private $line;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    private $hashCode;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    private $ip;

    /**
     * @var User|null
     *
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User",nullable=true)
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Exception
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Exception
     */
    public function setDate(\DateTime $date): Exception
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Exception
     */
    public function setCode(int $code): Exception
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Exception
     */
    public function setMessage(string $message): Exception
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return Exception
     */
    public function setFile(string $file): Exception
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @param int $line
     * @return Exception
     */
    public function setLine(int $line): Exception
    {
        $this->line = $line;
        return $this;
    }

    /**
     * @return string
     */
    public function getHashCode(): string
    {
        return $this->hashCode;
    }

    /**
     * @param string $hashCode
     * @return Exception
     */
    public function setHashCode(string $hashCode): Exception
    {
        $this->hashCode = $hashCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return Exception
     */
    public function setIp(string $ip): Exception
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return Exception
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }
}