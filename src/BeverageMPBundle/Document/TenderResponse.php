<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class TenderResponse
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\TenderResponseRepository")
 */
class TenderResponse
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var User
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var Tender
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\Tender")
     *
     * @Assert\NotBlank()
     */
    private $tender;

    /**
     * @var \DateTime
     * @MongoDB\Date
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $date;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     */
    private $response;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="0.0")
     */
    private $price;

    /**
     * @var integer
     * @MongoDB\Field(type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min="0")
     */
    private $quantity;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return TenderResponse
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return TenderResponse
     */
    public function setUser(User $user): TenderResponse
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Tender
     */
    public function getTender(): Tender
    {
        return $this->tender;
    }

    /**
     * @param Tender $tender
     * @return TenderResponse
     */
    public function setTender(Tender $tender): TenderResponse
    {
        $this->tender = $tender;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return TenderResponse
     */
    public function setDate(\DateTime $date): TenderResponse
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getResponse(): string
    {
        return $this->response;
    }

    /**
     * @param string $response
     * @return TenderResponse
     */
    public function setResponse(string $response): TenderResponse
    {
        $this->response = $response;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return TenderResponse
     */
    public function setPrice(float $price): TenderResponse
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return TenderResponse
     */
    public function setQuantity(int $quantity): TenderResponse
    {
        $this->quantity = $quantity;
        return $this;
    }
}