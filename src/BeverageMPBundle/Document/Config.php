<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Config
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\ConfigRepository")
 */
class Config
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     */
    private $parameterName;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $value = null;

    /**
     * @var \DateTime
     * @MongoDB\Date()
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $lastEdit;

    /**
     * @var User
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User",nullable=true)
     */
    private $by = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Config
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getParameterName(): string
    {
        return $this->parameterName;
    }

    /**
     * @param string $parameterName
     * @return Config
     */
    public function setParameterName(string $parameterName): Config
    {
        $this->parameterName = $parameterName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Config
     */
    public function setValue(string $value): Config
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastEdit(): \DateTime
    {
        return $this->lastEdit;
    }

    /**
     * @param \DateTime $lastEdit
     * @return Config
     */
    public function setLastEdit(\DateTime $lastEdit): Config
    {
        $this->lastEdit = $lastEdit;
        return $this;
    }

    /**
     * @return User
     */
    public function getBy(): User
    {
        return $this->by;
    }

    /**
     * @param User $by
     * @return Config
     */
    public function setBy(User $by): Config
    {
        $this->by = $by;
        return $this;
    }
}