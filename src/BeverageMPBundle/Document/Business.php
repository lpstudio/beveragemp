<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Algolia\AlgoliaSearchBundle\Mapping\Annotation as Algolia;

/**
 * Class Business
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\BusinessRepository")
 */
class Business
{
    /**
     * @MongoDB\Id
     *
     * @Algolia\Attribute(algoliaName="id")
     */
    private $id;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Algolia\Attribute()
     */
    private $name;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     *
     * @Algolia\Attribute()
     */
    private $commercialName = null;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $phone;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $website = null;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $email = null;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Algolia\Attribute()
     */
    private $address;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Algolia\Attribute()
     */
    private $city;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Algolia\Attribute()
     */
    private $zip;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Algolia\Attribute()
     */
    private $state;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Algolia\Attribute()
     */
    private $country;

    /**
     * @var User
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Algolia\Attribute()
     */
    private $primaryContact;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Business
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Business
     */
    public function setName(string $name): Business
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCommercialName()
    {
        return $this->commercialName;
    }

    /**
     * @param string $commercialName
     * @return Business
     */
    public function setCommercialName(string $commercialName): Business
    {
        $this->commercialName = $commercialName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Business
     */
    public function setPhone(string $phone): Business
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     * @return Business
     */
    public function setWebsite(string $website): Business
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Business
     */
    public function setEmail(string $email): Business
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Business
     */
    public function setAddress(string $address): Business
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Business
     */
    public function setCity(string $city): Business
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return Business
     */
    public function setZip(string $zip): Business
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return Business
     */
    public function setState(string $state): Business
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return Business
     */
    public function setCountry(string $country): Business
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return User
     */
    public function getPrimaryContact(): User
    {
        return $this->primaryContact;
    }

    /**
     * @param User $primaryContact
     * @return Business
     */
    public function setPrimaryContact(User $primaryContact): Business
    {
        $this->primaryContact = $primaryContact;
        return $this;
    }
}