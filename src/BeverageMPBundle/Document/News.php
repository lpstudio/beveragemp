<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class News
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\NewsRepository")
 */
class News
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var User
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var string
     * @MongoDB\Field(type="string",)
     *
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @var \DateTime
     * @MongoDB\Date
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    private $date;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     */
    private $picture;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return News
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return News
     */
    public function setUser(User $user): News
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return News
     */
    public function setTitle(string $title): News
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return News
     */
    public function setContent(string $content): News
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return News
     */
    public function setDate(\DateTime $date): News
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return News
     */
    public function setPicture(string $picture): News
    {
        $this->picture = $picture;
        return $this;
    }
}