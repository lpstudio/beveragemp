<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EmailValidation
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\EmailValidationRepository")
 */
class EmailValidation
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var User
     *
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return EmailValidation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return EmailValidation
     */
    public function setUser(User $user): EmailValidation
    {
        $this->user = $user;
        return $this;
    }
}