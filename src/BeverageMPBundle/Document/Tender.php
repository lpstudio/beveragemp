<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Algolia\AlgoliaSearchBundle\Mapping\Annotation as Algolia;

/**
 * Class Tender
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\TenderRepository")
 */
class Tender
{
    /**
     * @MongoDB\Id
     *
     * @Algolia\Attribute(algoliaName="tenderID")
     */
    private $id;

    /**
     * @var User
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var \DateTime
     * @MongoDB\Date
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     *
     * @Algolia\Attribute
     */
    private $publication;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $title;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $description;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $tinyDesc;

    /**
     * @var integer
     * @MongoDB\Field(type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min="0")
     *
     * @Algolia\Attribute
     */
    private $quantity;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="0.0")
     *
     * @Algolia\Attribute
     */
    private $priceMin;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="0.0")
     *
     * @Algolia\Attribute
     */
    private $priceMax;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $currency;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $location;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Tender
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Tender
     */
    public function setUser(User $user): Tender
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublication(): \DateTime
    {
        return $this->publication;
    }

    /**
     * @param \DateTime $publication
     * @return Tender
     */
    public function setPublication(\DateTime $publication): Tender
    {
        $this->publication = $publication;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Tender
     */
    public function setTitle(string $title): Tender
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Tender
     */
    public function setDescription(string $description): Tender
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getTinyDesc(): string
    {
        return $this->tinyDesc;
    }

    /**
     * @param string $tinyDesc
     * @return Tender
     */
    public function setTinyDesc(string $tinyDesc): Tender
    {
        $this->tinyDesc = $tinyDesc;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Tender
     */
    public function setQuantity(int $quantity): Tender
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceMin(): float
    {
        return $this->priceMin;
    }

    /**
     * @param float $priceMin
     * @return Tender
     */
    public function setPriceMin(float $priceMin): Tender
    {
        $this->priceMin = $priceMin;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceMax(): float
    {
        return $this->priceMax;
    }

    /**
     * @param float $priceMax
     * @return Tender
     */
    public function setPriceMax(float $priceMax): Tender
    {
        $this->priceMax = $priceMax;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Tender
     */
    public function setCurrency(string $currency): Tender
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * This method generates the tiny description
     * @param string $desc
     */
    public function generateTinyDesc(bool $custom = false, string $desc = null)
    {
        if ($custom){
            $this -> tinyDesc = $desc;
        } else {
            $this -> tinyDesc = substr($this -> description,0,140) . '...';
        }
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     * @return Tender
     */
    public function setLocation(string $location): Tender
    {
        $this->location = $location;
        return $this;
    }
}