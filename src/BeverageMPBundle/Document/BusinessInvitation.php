<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class BusinessInvitation
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document()
 */
class BusinessInvitation
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var Business
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\Business")
     */
    private $business;

    /**
     * @var \DateTime
     * @MongoDB\Date
     */
    private $date;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     */
    private $email;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BusinessInvitation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Business
     */
    public function getBusiness(): Business
    {
        return $this->business;
    }

    /**
     * @param Business $business
     * @return BusinessInvitation
     */
    public function setBusiness(Business $business): BusinessInvitation
    {
        $this->business = $business;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return BusinessInvitation
     */
    public function setDate(\DateTime $date): BusinessInvitation
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return BusinessInvitation
     */
    public function setEmail(string $email): BusinessInvitation
    {
        $this->email = $email;
        return $this;
    }
}