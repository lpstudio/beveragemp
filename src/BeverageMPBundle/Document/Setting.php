<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Setting
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\SettingRepository")
 */
class Setting
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var User
     *
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var boolean
     *
     * @MongoDB\Field(type="boolean")
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="boolean")
     */
    private $enable2fa = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Setting
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Setting
     */
    public function setUser(User $user): Setting
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnable2fa(): bool
    {
        return $this->enable2fa;
    }

    /**
     * @param bool $enable2fa
     * @return Setting
     */
    public function setEnable2fa(bool $enable2fa): Setting
    {
        $this->enable2fa = $enable2fa;
        return $this;
    }
}