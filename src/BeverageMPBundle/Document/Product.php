<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use Algolia\AlgoliaSearchBundle\Mapping\Annotation as Algolia;

/**
 * Class Product
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\ProductRepository")
 */
class Product
{
    /**
     * @MongoDB\Id
     *
     * @Algolia\Attribute(algoliaName="productID")
     */
    private $id;

    /**
     * @var User
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\User")
     *
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $title;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $tinyDesc;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $description;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="0.0")
     *
     * @Algolia\Attribute
     */
    private $price;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $currency;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $picture = 'blank.png';

    /**
     * @var \DateTime
     * @MongoDB\Date
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     *
     * @Algolia\Attribute
     */
    private $date;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $variety;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $origin;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute
     */
    private $format;

    /**
     * @var float
     * @MongoDB\Field(type="float")
     *
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min="0.0",max="100.0")
     *
     * @Algolia\Attribute
     */
    private $alc;

    /**
     * @var int
     * @MongoDB\Field(type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min="0")
     */
    private $score = 0;

    /**
     * @var bool
     * @MongoDB\Field(type="boolean")
     *
     * @Assert\NotBlank()
     */
    private $featured = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Product
     */
    public function setUser(User $user): Product
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Product
     */
    public function setTitle(string $title): Product
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTinyDesc(): string
    {
        return $this->tinyDesc;
    }

    /**
     * @param string $tinyDesc
     * @return Product
     */
    public function setTinyDesc(string $tinyDesc): Product
    {
        $this->tinyDesc = $tinyDesc;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Product
     */
    public function setCurrency(string $currency): Product
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     * @return Product
     */
    public function setPicture(string $picture): Product
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Product
     */
    public function setDate(\DateTime $date): Product
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getVariety(): string
    {
        return $this->variety;
    }

    /**
     * @param string $variety
     * @return Product
     */
    public function setVariety(string $variety): Product
    {
        $this->variety = $variety;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     * @return Product
     */
    public function setOrigin(string $origin): Product
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return Product
     */
    public function setFormat(string $format): Product
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return float
     */
    public function getAlc(): float
    {
        return $this->alc;
    }

    /**
     * @param float $alc
     * @return Product
     */
    public function setAlc(float $alc): Product
    {
        $this->alc = $alc;
        return $this;
    }

    /**
     * This method generates the tiny description
     * @param string $desc
     */
    public function generateTinyDesc(bool $custom = false, string $desc = null)
    {
        if ($custom){
            $this -> tinyDesc = $desc;
        } else {
            $this -> tinyDesc = substr($this -> description,0,140) . '...';
        }
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     * @return Product
     */
    public function setScore(int $score): Product
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     * @return Product
     */
    public function setFeatured(bool $featured): Product
    {
        $this->featured = $featured;
        return $this;
    }
}