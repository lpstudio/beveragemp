<?php

namespace BeverageMPBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Algolia\AlgoliaSearchBundle\Mapping\Annotation as Algolia;

/**
 * Class User
 * @package BeverageMPBundle\Document
 *
 * @MongoDB\Document(repositoryClass="BeverageMPBundle\DocumentRepository\UserRepository")
 */
class User implements AdvancedUserInterface, \Serializable, EquatableInterface
{
    public static $ROLE_USER = 'ROLE_USER';
    public static $ROLE_PRODUCER = 'ROLE_PRODUCER';
    public static $ROLE_ADMIN = 'ROLE_ADMIN';
    public static $ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    public static $ROLE_FULL_ACCESS_MONTHLY = 'ROLE_FULL_ACCESS_MONTHLY';
    public static $ROLE_FULL_ACCESS_YEARLY = 'ROLE_FULL_ACCESS_YEARLY';

    public static $TYPE_PRODUCER = array(
        "Winery",
        "Distillery",
        "Breweries",
        "Cidrerie",
        "Negociant/Exporter",
        "Agent/Broker",
    );
    public static $TYPE_USER = array(
        "Wine Importer",
        "Spirit Importer",
        "Cider Importer",
        "Beer Importer",
        "Wine Wholesaler",
        "Spirit Wholesaler",
        "Cider Wholesaler",
        "Beer Wholesaler",
        "Negociant/Importer",
    );

    /**
     * @MongoDB\Id
     * @Algolia\Id
     */
    private $id;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     *
     * @Assert\NotBlank()
     */
    private $salt;

    /**
     * @var array
     * @MongoDB\Collection
     * @Algolia\Attribute
     *
     * @Assert\NotBlank()
     */
    private $roles;

    /**
     * @var bool
     * @MongoDB\Field(type="boolean")
     */
    private $validated = false;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Algolia\Attribute
     *
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Algolia\Attribute
     *
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $phone = null;

    /**
     * @var \DateTime
     * @MongoDB\Date
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $registration;

    /**
     * @var string
     * @MongoDB\Field(type="string")
     * @Algolia\Attribute
     *
     * @Assert\NotBlank()
     */
    private $profilePic = 'blank.png';

    /**
     * @var \DateTime|null
     * @MongoDB\Date(nullable=true)
     *
     * @Assert\DateTime()
     */
    private $lastConnect = null;

    /**
     * @var boolean
     * @MongoDB\Field(type="boolean")
     *
     * @Assert\NotBlank()
     */
    private $isEnabled = true;

    /**
     * @var boolean
     * @MongoDB\Field(type="boolean")
     *
     * @Assert\NotBlank()
     */
    private $isNonLocked = true;

    /**
     * @var string|null
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $twoFASecret;

    /**
     * @var string|null
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $customerId;

    /**
     * @var Business
     * @MongoDB\ReferenceOne(targetDocument="BeverageMPBundle\Document\Business",nullable=true)
     *
     * @Assert\NotBlank()
     *
     * @Algolia\Attribute()
     */
    private $business;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     */
    private $function = null;

    /**
     * @var string
     * @MongoDB\Field(type="string",nullable=true)
     *
     * @Algolia\Attribute()
     */
    private $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt(string $salt): User
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @return array
     */
    public function getRole(): array
    {
        return $this->roles;
    }

    /**
     * @param string $role
     * @return User
     */
    public function addRole(string $role): User
    {
        $this->roles[] = $role;
        return $this;
    }

    /**
     * @param string $role
     * @return User
     */
    public function removeRole(string $role): User
    {
        unset($this -> roles[$role]);
        return $this;
    }

    /**
     * This method resets the role of an user
     */
    public function resetRoles(){
        if (in_array($this -> type, static::$TYPE_PRODUCER)){
            $this -> roles = array(static::$ROLE_PRODUCER);
        } else if (in_array($this -> type, static::$TYPE_USER)){
            $this -> roles = array(static::$ROLE_USER);
        } else {
            if (in_array(static::$ROLE_ADMIN, $this -> getRoles())){
                $this -> roles = array(static::$ROLE_ADMIN);
            } elseif (in_array(static::$ROLE_SUPER_ADMIN, $this -> getRoles())){
                $this -> roles = array(static::$ROLE_SUPER_ADMIN);
            }
        }
    }

    /**
     * @return bool
     */
    public function isValidated(): bool
    {
        return $this->validated;
    }

    /**
     * @param bool $validated
     * @return User
     */
    public function setValidated(bool $validated): User
    {
        $this->validated = $validated;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName(string $firstName): User
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName): User
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return User
     */
    public function setPhone(string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegistration(): \DateTime
    {
        return $this->registration;
    }

    /**
     * @param \DateTime $registration
     * @return User
     */
    public function setRegistration(\DateTime $registration): User
    {
        $this->registration = $registration;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfilePic(): string
    {
        return $this->profilePic;
    }

    /**
     * @param string $profilePic
     */
    public function setProfilePic(string $profilePic)
    {
        $this->profilePic = $profilePic;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastConnect()
    {
        return $this->lastConnect;
    }

    /**
     * @param \DateTime $lastConnect
     * @return User
     */
    public function setLastConnect(\DateTime $lastConnect): User
    {
        $this->lastConnect = $lastConnect;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNonLocked(): bool
    {
        return $this->isNonLocked;
    }

    /**
     * @param bool $isNonLocked
     * @return User
     */
    public function setIsNonLocked(bool $isNonLocked): User
    {
        $this->isNonLocked = $isNonLocked;
        return $this;
    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this -> id,
            $this -> email,
            $this -> password,
            $this -> salt,
            $this -> isEnabled
        ));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this -> id,
            $this -> email,
            $this -> password,
            $this -> salt,
            $this -> isEnabled
            ) = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this -> roles;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this -> getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
        return;
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return $this -> isNonLocked;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this -> isEnabled;
    }

    /**
     * @return string|null
     */
    public function getTwoFASecret()
    {
        return $this->twoFASecret;
    }

    /**
     * @param string $twoFASecret
     * @return User
     */
    public function setTwoFASecret(string $twoFASecret): User
    {
        $this->twoFASecret = $twoFASecret;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param null|string $customerId
     * @return User
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * Also implementation should consider that $user instance may implement
     * the extended user interface `AdvancedUserInterface`.
     *
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user)
    {
        if ($user instanceof User){
            //checking roles are the same
            $isEqual = count($this -> getRoles()) == count($user -> getRoles());
            if ($isEqual){
                foreach ($this -> getRoles() as $role){
                    $isEqual = $isEqual && in_array($role,$user -> getRoles());
                }
            }
            return $isEqual;
        }
        return false;
    }

    /**
     * @return Business|null
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @param Business $business
     * @return User
     */
    public function setBusiness(Business $business): User
    {
        $this->business = $business;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param string $function
     * @return User
     */
    public function setFunction(string $function): User
    {
        $this->function = $function;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return User
     */
    public function setType(string $type): User
    {
        $this->type = $type;
        return $this;
    }
}