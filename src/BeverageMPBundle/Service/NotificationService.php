<?php

namespace BeverageMPBundle\Service;

use BeverageMPBundle\Document\Tender;
use BeverageMPBundle\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Routing\Router;

class NotificationService
{
    protected $documentManager, $templating, $mailer, $router;

    public function __construct(DocumentManager $documentManager, \Twig_Environment $environment, \Swift_Mailer $mailer, Router $router)
    {
        $this -> documentManager = $documentManager;
        $this -> templating = $environment;
        $this -> mailer = $mailer;
        $this -> router = $router;
    }

    public function sendValidationMail(User $user, string $validationLink)
    {
        if ($user != null || $validationLink != null){
            $message = new \Swift_Message();
            $message -> setTo($user -> getEmail())
                -> setSubject('Validation')
                -> setFrom('noreply@thebeveragemarket.com')
                -> setBody($this -> templating -> render('@BeverageMP/Emails/validation.html.twig', array(
                    'firstname' => $user -> getFirstName(),
                    'validation_link' => $validationLink
                )),'text/html');
            $this -> mailer -> send($message);
        } else {
            throw new \Exception('A parameter is missing.');
        }
    }

    public function sendResetPasswordEmail(User $user, string $resetLink)
    {
        if ($user != null || $resetLink != null){
            $message = new \Swift_Message();
            $message -> setTo($user -> getEmail())
                -> setSubject('Password reset')
                -> setFrom('noreply@thebeveragemarket.com')
                -> setBody($this -> templating -> render('@BeverageMP/Emails/password-reset.html.twig', array(
                    'firstname' => $user -> getFirstName(),
                    'reset_link' => $resetLink
                )), 'text/html');
            $this -> mailer -> send($message);
        } else {
            throw new \Exception('A parameter is missing.');
        }
    }

    public function sendAnswerNotification(User $user, Tender $tender)
    {
        if ($user != null && $tender != null){
            $message = new \Swift_Message();
            $message -> setTo($user -> getEmail())
                -> setSubject('New answer')
                -> setFrom('noreply@thebeveragemarket.com')
                -> setBody($this -> templating -> render('@BeverageMP/Emails/answer-tender.html.twig', array(
                    'firstname' => $user -> getFirstName(),
                    'tender' => $tender
                )),'text/html');
            $this -> mailer -> send($message);
        } else {
            throw new \Exception('A parameter is missing.');
        }
    }

    public function sendContactEmail(string $name, string $email, string $phone, string $company, string $messsage)
    {
        $mess = new \Swift_Message();
        $mess -> setTo('contact@thebeveragemarket.com')
            -> setFrom($email)
            -> setSubject('Web contact')
            -> setBody($this -> templating -> render('@BeverageMP/Emails/contact.html.twig', array(
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'company' => $company,
                'message' => $messsage
            )),'text/html');
        $this -> mailer -> send($mess);
    }

    public function sendCompanyInvite(User $user, string $destEmail, string $inviteLink)
    {
        $message = new \Swift_Message();
        $message -> setTo($destEmail)
            -> setFrom('noreply@thebeveragemarket.com')
            -> setSubject('Company invitation')
            -> setBody($this -> templating -> render('@BeverageMP/Emails/company-invite.html.twig', array(
                'fullname' => $user -> getFirstName() . ' ' . $user -> getLastName(),
                'company' => $user -> getBusiness() -> getName(),
                'invite_link' => $inviteLink
            )), 'text/html');
        $this -> mailer -> send($message);
    }

    public function sendBusinessContact(User $toUser, User $fromUser, $message)
    {
        $message = new \Swift_Message();
        $message -> setTo($toUser -> getEmail())
            -> setFrom($fromUser -> getEmail())
            -> setSubject('Company contact')
            -> setBody($this -> templating -> render('@BeverageMP/Emails/business-contact.html.twig', array(
                'fullname' => $fromUser -> getFirstName(),
                'company' => $fromUser -> getBusiness() -> getName(),
                'message' => $message
            )), 'text/html');
        $this -> mailer -> send($message);
    }
}