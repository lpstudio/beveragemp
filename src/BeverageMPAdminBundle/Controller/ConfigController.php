<?php

namespace BeverageMPAdminBundle\Controller;

use BeverageMPBundle\Document\Config;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConfigController extends Controller
{
    public function editAction(Request $request, string $parameterName)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $configRepo = $mongoMan -> getRepository('BeverageMPBundle:Config');

        //checking fields
        if ($request -> get($parameterName . '_value') != null){

            //loading config by parameter name
            $config = $configRepo -> findOneBy(array(
                'parameterName' => $parameterName
            ));
            if ($config == null){
                $config = new Config();
                $config -> setParameterName($parameterName)
                    -> setLastEdit(new \DateTime('now'))
                    -> setBy($this -> getUser())
                    -> setValue($request -> get($parameterName . '_value'));
                $mongoMan -> persist($config);
                $mongoMan -> flush();

                return $this -> redirectToRoute('beverage_mp_admin_homepage', array(
                    'success' => true
                ));
            }

            //editing config
            $config -> setValue($request -> get($parameterName . '_value'))
                -> setBy($this -> getUser())
                -> setLastEdit(new \DateTime('now'));
            $mongoMan -> persist($config);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_homepage', array(
                'success' => true
            ));
        } else {
            return $this -> redirectToRoute('beverage_mp_admin_homepage', array(
                'error' => true,
                'type' => 1
            ));
        }
    }
}