<?php

namespace BeverageMPAdminBundle\Controller;

use BeverageMPBundle\Document\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
    public function publishAction(Request $request)
    {
        //checking fields
        if ($request -> get('title') != null && $request -> get('content') != null){
            $title = $request -> get('title');
            $content = $request -> get('content');

            //creating manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //getting repo
            $newsRepo = $mongoMan -> getRepository('BeverageMPBundle:News');

            //setting picture
            if ($request -> files -> get('image') != null){
                $image = $request -> files -> get('image');

                //checking errors
                if ($image -> getError() > 0){
                    return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                        'error' => true,
                        'type' => $image -> getError()
                    ));
                }

                //checking if file is valid
                if ($image -> isValid()){

                    //checking mime type
                    if ($image -> getMimeType() != 'image/gif' && $image -> getMimeType() != 'image/png'
                    && $image -> getMimeType() != 'image/jpeg'){
                        return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                            'error' => true,
                            'type' => 4
                        ));
                    }

                    //upload dir
                    if ($this -> container -> getParameter('kernel.environment') == 'dev'){
                        $projectDir = str_replace('/app_dev.php','',$request -> server -> get('SCRIPT_FILENAME'));
                    } else {
                        $projectDir = str_replace('/app.php','',$request -> server -> get('SCRIPT_FILENAME'));
                    }
                    $uploadDir = $projectDir . '/uploads/news/';

                    //renaming file
                    $filename = substr(md5(uniqid(mt_rand(), true)),0,8);
                    $filename = strtolower($filename . '.' . $image -> guessExtension());

                    //moving profile pic to dir
                    $image -> move($uploadDir,$filename);
                } else {
                    return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                        'error' => true,
                        'type' => 5
                    ));
                }

            } else {
                return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                    'error' => true,
                    'type' => 1
                ));
            }

            //creating news
            $new = new News();
            $new -> setTitle($title)
                -> setContent($content)
                -> setDate(new \DateTime('now'))
                -> setUser($this -> getUser())
                -> setPicture($filename);

            //validating new
            $errors = $this -> get('validator') -> validate($new);
            if (count($errors) > 0){
                throw new \Exception((string)$errors);
            }

            //saving
            $mongoMan -> persist($new);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                'success' => true
            ));
        } else {
            return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                'error' => true,
                'type' => 1
            ));
        }
    }

    /**
     * Delete a news
     * @param string $newsId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(string $newsId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $newsRepo = $mongoMan -> getRepository('BeverageMPBundle:News');

        //checking news existence
        $news = $newsRepo -> find($newsId);
        if ($news == null){
            throw $this -> createNotFoundException();
        }

        //delete news
        $mongoMan -> remove($news);
        $mongoMan -> flush();

        return $this -> redirectToRoute('beverage_mp_admin_list_news', array(
            'success' => true
        ));
    }

    public function editAction(Request $request, string $newsId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $newsRepo = $mongoMan -> getRepository('BeverageMPBundle:News');

        //checking news existence
        $news = $newsRepo -> find($newsId);
        if ($news == null){
            throw $this -> createNotFoundException();
        }

        //checking fields
        if ($request -> get('title') != null && $request -> get('content') != null){
            $title = $request -> get('title');
            $content = $request -> get('content');

            $news -> setTitle($title)
                -> setContent($content);

            //upload dir
            if ($this -> container -> getParameter('kernel.environment') == 'dev'){
                $projectDir = str_replace('/app_dev.php','',$request -> server -> get('SCRIPT_FILENAME'));
            } else {
                $projectDir = str_replace('/app.php','',$request -> server -> get('SCRIPT_FILENAME'));
            }
            $uploadDir = $projectDir . '/uploads/news/';

            unlink($uploadDir . $news -> getPicture());

            //setting picture
            if ($request -> files -> get('image') != null){
                $image = $request -> files -> get('image');

                //checking errors
                if ($image -> getError() > 0){
                    return $this -> redirectToRoute('beverage_mp_admin_publish_news', array(
                        'error' => true,
                        'type' => $image -> getError()
                    ));
                }

                //checking if file is valid
                if ($image -> isValid()){

                    //checking mime type
                    if ($image -> getMimeType() != 'image/gif' && $image -> getMimeType() != 'image/png'
                        && $image -> getMimeType() != 'image/jpeg'){
                        return $this -> redirectToRoute('beverage_mp_admin_edit_news', array(
                            'error' => true,
                            'type' => 4
                        ));
                    }

                    //upload dir
                    if ($this -> container -> getParameter('kernel.environment') == 'dev'){
                        $projectDir = str_replace('/app_dev.php','',$request -> server -> get('SCRIPT_FILENAME'));
                    } else {
                        $projectDir = str_replace('/app.php','',$request -> server -> get('SCRIPT_FILENAME'));
                    }
                    $uploadDir = $projectDir . '/uploads/news/';

                    //renaming file
                    $filename = substr(md5(uniqid(mt_rand(), true)),0,8);
                    $filename = strtolower($filename . '.' . $image -> guessExtension());

                    //moving profile pic to dir
                    $image -> move($uploadDir,$filename);

                    $news -> setPicture($filename);

                    $mongoMan -> persist($news);
                    $mongoMan -> flush();

                    return $this -> redirectToRoute('beverage_mp_admin_edit_news', array(
                        'newsId' => $newsId,
                        'success' => true
                    ));
                } else {
                    return $this -> redirectToRoute('beverage_mp_admin_edit_news', array(
                        'error' => true,
                        'type' => 5
                    ));
                }

            } else {
                return $this -> redirectToRoute('beverage_mp_admin_edit_news', array(
                    'error' => true,
                    'type' => 1
                ));
            }
        }
    }
}