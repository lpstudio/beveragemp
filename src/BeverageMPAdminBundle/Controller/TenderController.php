<?php

namespace BeverageMPAdminBundle\Controller;

use BeverageMPBundle\Document\Tender;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TenderController extends Controller
{
    /**
     * This method deletes a tender specified by its id
     * @param string $tenderId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(string $tenderId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');

        //checking tender
        $tender = $tenderRepo -> find($tenderId);
        if ($tender != null){

            //delete tender
            $mongoMan -> remove($tender);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_list_tenders', array(
                'success' => true
            ));
        } else {
            throw $this -> createNotFoundException();
        }
    }

    public function addAction(Request $request)
    {
        if ($request -> get('title') != null && $request -> get('quantity') != null
            && $request -> get('pricemin') != null && $request -> get('pricemax') != null
            && $request -> get('currency') != null && $request -> get('description') != null
            && $request -> get('location') != null){

            $title = $request -> get('title');
            $description = $request -> get('description');
            $quantity = $request -> get('quantity');
            $priceMin = $request -> get('pricemin');
            $priceMax = $request -> get('pricemax');
            $currency = $request -> get('currency');
            $location = $request -> get('location');

            //create manager
            $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

            //creating tender
            $tender = new Tender();
            $tender -> setUser($this -> getUser())
                -> setCurrency($currency)
                -> setLocation($location)
                -> setTitle($title)
                -> setDescription($description)
                -> setPriceMin($priceMin)
                -> setPriceMax($priceMax)
                -> setQuantity($quantity)
                -> setPublication(new \DateTime('now'));

            if ($request -> get('tinydesc') != null){
                $tender -> generateTinyDesc(true,$request -> get('tinydesc'));
            } else {
                $tender -> generateTinyDesc();
            }

            //validating tender
            $validator = $this -> get('validator');
            $errors = $validator -> validate($tender);
            if (count($errors) > 0){
                throw new \Exception((string)$errors);
            }

            //saving tender
            $mongoMan -> persist($tender);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_add_tender',array(
                'success' => true
            ));
        } else {
            return $this -> redirectToRoute('beverage_mp_admin_add_tender', array(
                'error' => true,
                'type' => 1
            ));
        }
    }
}