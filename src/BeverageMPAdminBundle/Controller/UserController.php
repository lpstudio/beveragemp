<?php

namespace BeverageMPAdminBundle\Controller;

use BeverageMPBundle\Document\Business;
use BeverageMPBundle\Document\EmailValidation;
use BeverageMPBundle\Document\User;
use BeverageMPBundle\Utils\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * This method deletes an user
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(string $userId)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repository
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){
            //checking user role
            if ($user -> getRole() != User::$ROLE_ADMIN && $user -> getRole() != User::$ROLE_SUPER_ADMIN){
                //delete user
                $mongoMan -> remove($user);
                $mongoMan -> flush();

                return $this -> redirectToRoute('beverage_mp_admin_list_users', array(
                    'success' => true
                ));
            }

            return $this -> redirectToRoute('beverage_mp_admin_list_users');
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method locks the account of an user.
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function lockAction(string $userId)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repository
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){
            //checking user role
            if ($user -> getRole() != User::$ROLE_ADMIN && $user -> getRole() != User::$ROLE_SUPER_ADMIN){
                //locking user
                $user -> setIsNonLocked(false);
                $mongoMan -> persist($user);
                $mongoMan -> flush();
            }

            return $this -> redirectToRoute('beverage_mp_admin_list_users');
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method unlocks the account of an user
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function unlockAction(string $userId)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repository
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){
            //checking user role
            if ($user -> getRole() != User::$ROLE_ADMIN && $user -> getRole() != User::$ROLE_SUPER_ADMIN){
                //unlocking user
                $user -> setIsNonLocked(true);
                $mongoMan -> persist($user);
                $mongoMan -> flush();
            }

            return $this -> redirectToRoute('beverage_mp_admin_list_users');
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method displays the info of an user
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(string $userId)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repository
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){
            return $this -> render('@BeverageMPAdmin/Default/view-user.html.twig',array(
                'user' => $user
            ));
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method resets the user profile pic
     * @param Request $request
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetProfilePictureAction(Request $request, string $userId)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repository
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){
            //upload dir
            if ($this -> container -> getParameter('kernel.environment') == 'dev'){
                $projectDir = str_replace('/app_dev.php','',$request -> server -> get('SCRIPT_FILENAME'));
            } else {
                $projectDir = str_replace('/app.php','',$request -> server -> get('SCRIPT_FILENAME'));
            }
            $uploadDir = $projectDir . '/uploads/profile/';

            //deleting old profile pic if it is not blank.png
            if ($user -> getProfilePic() != 'blank.png'){
                unlink($uploadDir . $user -> getProfilePic());
            }

            //setting user profile pic to blank
            $user -> setProfilePic('blank.png');
            $mongoMan -> persist($user);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_user_info', array(
                'userId' => $userId,
                'success' => true
            ));
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This methods resends a validation mail to an user and
     * set it to not valid.
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function resendValidationEmailAction(string $userId)
    {
        //create manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repository
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){
            //sending validation
            $validation = new EmailValidation();
            $validation -> setUser($user);

            //validating validation
            $validator = $this -> get('validator');
            $errors = $validator -> validate($validation);
            if (count($errors) > 0){
                $errorStrings = (string)$errors;
                throw new \Exception($errorStrings);
            }

            //saving validation
            $mongoMan -> persist($validation);
            $mongoMan -> flush();

            //creating validation link
            $link = 'https://www.thebeveragemarket.com' . $this -> generateUrl('beverage_mp_validate_email', array(
                'validationId' => $validation -> getId()
            ));

            //sending email
            $this -> get('beverage_mp.notificator') -> sendValidationMail($user,$link);

            //setting user as not validated
            $user -> setValidated(false);
            $mongoMan -> persist($user);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_user_info', array(
                'userId' => $userId,
                'success' => true
            ));
        } else {
            throw $this -> createNotFoundException();
        }
    }

    /**
     * This method makes an user admin
     * @param string $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function makeAdminAction(string $userId)
    {
        $this -> denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');

        //checking user
        $user = $userRepo -> find($userId);
        if ($user != null){

            //make user admin
            $user -> setRole(User::$ROLE_ADMIN);
            $mongoMan -> persist($user);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_list_users');
        } else {
            throw $this -> createNotFoundException();
        }
    }

    public function addAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repos
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');

        //checking is adding user to a company
        if ($request -> get('company') != null){

            //checking fields
            if ($request -> get('firstname') != null && $request -> get('lastname') != null
            && $request -> get('email') != null && $request -> get('password') != null
            && $request -> get('type') != null){

                //creating vars
                $firstName = $request -> get('firstname');
                $lastName = $request -> get('lastname');
                $email = $request -> get('email');
                $password = $request -> get('password');
                $type = $request -> get('type');

                //checking user existence
                $user = $userRepo -> findOneBy(array(
                    'email' => $email
                ));
                if ($user != null){
                    return $this -> redirectToRoute('beverage_mp_admin_add_user', array(
                        'error' => true,
                        'type' => 2
                    ));
                }

                //checking business existence
                $company = $businessRepo -> find($request -> get('company'));
                if ($company == null){
                    return $this -> redirectToRoute('beverage_mp_admin_add_user', array(
                        'error' => true,
                        'type' => 2
                    ));
                }

                //creating user
                $user = new User();
                $user -> setFirstName($firstName)
                    -> setLastName($lastName)
                    -> setEmail($email)
                    -> setValidated(true)
                    -> setRegistration(new \DateTime('now'))
                    -> setBusiness($company)
                    -> setType($type);

                //setting role
                if (in_array($type, User::$TYPE_PRODUCER)){
                    $user -> addRole(User::$ROLE_PRODUCER);
                } elseif (in_array($type, User::$TYPE_USER)){
                    $user -> addRole(User::$ROLE_USER);
                } else {
                    $user -> addRole(User::$ROLE_USER);
                }

                //hashig password
                $salt = Security::generateSalt();
                $password = password_hash($password, PASSWORD_BCRYPT, array('salt' => $salt));
                $user -> setSalt($salt)
                    -> setPassword($password);

                //optionnal fields
                if ($request -> get('function') != null){
                    $user -> setFunction($request -> get('function'));
                }

                if ($request -> get('phone') != null){
                    $user -> setPhone($request -> get('phone'));
                }

                //saving
                $mongoMan -> persist($user);
                $mongoMan -> flush();

                return $this -> redirectToRoute('beverage_mp_admin_add_user', array(
                    'success' => true
                ));
            } else {
                return $this -> redirectToRoute('beverage_mp_admin_add_user', array(
                    'error' => true,
                    'type' => 1
                ));
            }
        } else {
            //checking fields
            if ($request -> get('firstname') != null && $request -> get('lastname') != null
                && $request -> get('email') != null && $request -> get('password') != null
                && $request -> get('type') != null && $request -> get('name') != null
                && $request -> get('companyphone') != null && $request -> get('address') != null
                && $request -> get('city') != null && $request -> get('state') != null
                && $request -> get('zip') != null && $request -> get('country') != null) {

                //creating vars
                $firstName = $request->get('firstname');
                $lastName = $request->get('lastname');
                $email = $request->get('email');
                $password = $request->get('password');
                $type = $request -> get('type');
                $name = $request->get('name');
                $companyPhone = $request->get('companyphone');
                $address = $request->get('address');
                $city = $request->get('city');
                $state = $request->get('state');
                $zip = $request->get('zip');
                $country = $request->get('country');

                //checking user existence
                $user = $userRepo->findOneBy(array(
                    'email' => $email
                ));
                if ($user != null) {
                    return $this->redirectToRoute('beverage_mp_admin_add_user', array(
                        'error' => true,
                        'type' => 2
                    ));
                }

                //checking business existence
                $company = $businessRepo->find($request->get('company'));
                if ($company != null) {
                    return $this->redirectToRoute('beverage_mp_admin_add_user', array(
                        'error' => true,
                        'type' => 2
                    ));
                }

                //creating user
                $user = new User();
                $user->setFirstName($firstName)
                    ->setLastName($lastName)
                    ->setEmail($email)
                    ->setValidated(true)
                    ->setRegistration(new \DateTime('now'))
                    ->setType($type);

                //setting role
                if (in_array($type, User::$TYPE_PRODUCER)){
                    $user -> addRole(User::$ROLE_PRODUCER);
                } elseif (in_array($type, User::$TYPE_USER)){
                    $user -> addRole(User::$ROLE_USER);
                } else {
                    $user -> addRole(User::$ROLE_USER);
                }

                //hashig password
                $salt = Security::generateSalt();
                $password = password_hash($password, PASSWORD_BCRYPT, array('salt' => $salt));
                $user->setSalt($salt)
                    ->setPassword($password);

                //optionnal fields
                if ($request->get('function') != null) {
                    $user->setFunction($request->get('function'));
                }

                if ($request->get('phone') != null) {
                    $user->setPhone($request->get('phone'));
                }

                //saving
                $mongoMan->persist($user);
                $mongoMan->flush();

                //creating company
                $company = new Business();
                $company->setName($name)
                    ->setPhone($companyPhone)
                    ->setAddress($address)
                    ->setCity($city)
                    ->setZip($zip)
                    ->setState($state)
                    ->setCountry($country)
                    ->setPrimaryContact($user);

                //optionnal fields
                if ($request->get('companyemail') != null) {
                    $company->setEmail($request->get('companyemail'));
                }

                if ($request->get('website') != null) {
                    $company->setWebsite($request->get('website'));
                }

                if ($request->get('commercialname') != null) {
                    $company->setCommercialName($request->get('commercialname'));
                }

                //saving company
                $mongoMan->persist($company);
                $mongoMan->flush();

                //updating user business
                $user->setBusiness($company);

                $mongoMan->persist($user);
                $mongoMan->flush();

                return $this->redirectToRoute('beverage_mp_admin_add_user', array(
                    'success' => true
                ));
            } else {
                return $this->redirectToRoute('beverage_mp_admin_add_user', array(
                    'error' => true,
                    'type' => 1
                ));
            }
        }
    }
}