<?php

namespace BeverageMPAdminBundle\Controller;

use BeverageMPBundle\Document\Config;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * This method renders the admin homepage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $productRepo = $mongoMan -> getRepository('BeverageMPBundle:Product');
        $tenderRepo = $mongoMan -> getRepository('BeverageMPBundle:Tender');
        $configRepo = $mongoMan -> getRepository('BeverageMPBundle:Config');

        //counting users
        $users = $userRepo -> findAll();
        $userCount = count($users);

        //counting products
        $products = $productRepo -> findAll();
        $productCount = count($products);

        //counting tenders
        $tenders = $tenderRepo -> findAll();
        $tenderCount = count($tenders);

        //getting config
        $homepageBlocCodeConfig = $configRepo -> findOneBy(array(
            'parameterName' => 'homepageBlocCode'
        ));
        if ($homepageBlocCodeConfig == null){
            $homepageBlocCodeConfig = new Config();
            $homepageBlocCodeConfig -> setParameterName('homepageBlocCode')
                -> setLastEdit(new \DateTime('now'))
                -> setBy($this -> getUser());
            $mongoMan -> persist($homepageBlocCodeConfig);
            $mongoMan -> flush();
        }

        return $this->render('BeverageMPAdminBundle:Default:index.html.twig', array(
            'userCount' => $userCount,
            'productCount' => $productCount,
            'tenderCount' => $tenderCount,
            'homepageBlocCode' => $homepageBlocCodeConfig
        ));
    }

    /**
     * This method renders the tender listing
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listNewsAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //creating query builder
        $query = $mongoMan -> createQueryBuilder('BeverageMPBundle:News');

        $pagination = $this -> get('knp_paginator') -> paginate(
            $query,
            $request -> get('page',1),
            5
        );

        return $this -> render('@BeverageMPAdmin/Default/list-news.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This method renders the list users page
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listUsersAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //creating query
        $queryBuilder = $mongoMan -> createQueryBuilder('BeverageMPBundle:User');

        //creating paginator
        $paginator = $this -> get('knp_paginator');
        $pagination = $paginator -> paginate(
            $queryBuilder,
            $request -> get('page',1),
            10
        );

        return $this -> render('@BeverageMPAdmin/Default/list-users.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This mehtod renders the page for publishing news
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function publishNewsAction()
    {
        return $this -> render('@BeverageMPAdmin/Default/publish-news.html.twig');
    }

    /**
     * This method lists the tenders
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listTendersAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //creating query
        $queryBuilder = $mongoMan -> createQueryBuilder('BeverageMPBundle:Tender');

        //creating paginator
        $paginator = $this -> get('knp_paginator');
        $pagination = $paginator -> paginate(
            $queryBuilder,
            $request -> get('page',1),
            5
        );

        return $this -> render('@BeverageMPAdmin/Default/list-tenders.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This method lists the products
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listProductsAction(Request $request)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //creating query
        $queryBuilder = $mongoMan -> createQueryBuilder('BeverageMPBundle:Product');

        //creating paginator
        $paginator = $this -> get('knp_paginator');
        $pagination = $paginator -> paginate(
            $queryBuilder,
            $request -> get('page',1),
            10
        );

        return $this -> render('BeverageMPAdminBundle:Default:list-products.html.twig', array(
            'pagination' => $pagination
        ));
    }

    /**
     * This method renders the add and user page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addUserAction()
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $businessRepo = $mongoMan -> getRepository('BeverageMPBundle:Business');

        //loading existent companies
        $companies = $businessRepo -> findAll();

        return $this -> render('@BeverageMPAdmin/Default/add-user.html.twig', array(
            'companies' => $companies
        ));
    }

    /**
     * This method renders the news editor
     * @param string $newsId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editNewsAction(string $newsId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $newsRepo = $mongoMan -> getRepository('BeverageMPBundle:News');

        $news = $newsRepo -> find($newsId);
        if ($news == null){
            throw $this -> createNotFoundException();
        }

        return $this -> render('@BeverageMPAdmin/Default/edit-news.html.twig', array(
            'news' => $news
        ));
    }

    /**
     * This method renders the add tender page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addTenderAction()
    {
        return $this -> render('@BeverageMPAdmin/Default/add-tender.html.twig');
    }
}
