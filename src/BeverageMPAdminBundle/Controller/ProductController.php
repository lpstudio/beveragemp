<?php

namespace BeverageMPAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /***
     * This method deletes a product
     * @param string $productId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(string $productId)
    {
        //creating manager
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();

        //getting repo
        $productRepo = $mongoMan -> getRepository('BeverageMPBundle:Product');

        //checking product
        $product = $productRepo -> find($productId);
        if ($product != null){

            //delete product
            $mongoMan -> remove($product);
            $mongoMan -> flush();

            return $this -> redirectToRoute('beverage_mp_admin_list_products', array(
                'success' => true
            ));
        } else {
            throw $this -> createNotFoundException();
        }
    }
}