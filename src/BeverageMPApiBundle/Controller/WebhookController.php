<?php

namespace BeverageMPApiBundle\Controller;

use Psr\Log\LoggerInterface;
use Stripe\Error\SignatureVerification;
use Stripe\Event;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\Webhook;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebhookController extends Controller
{
    /**
     * This method parses the webhook request and creates an event that is dispatched
     * in the other methods.
     * @param Request $request
     * @return Response
     */
    public function endpointAction(Request $request)
    {
        $logger = $this -> get('logger');
        $response = new Response();

        //setting stripe secret
        Stripe::setApiKey($this -> getParameter('stripe_secret_key'));

        //getting data
        $payload = $request -> getContent();
        $signatureHeader = $request -> server -> get('HTTP_STRIPE_SIGNATURE');
        $event = null;

        try {
            $event = Webhook::constructEvent(
                $payload, $signatureHeader, $this -> getParameter('stripe_endpoint_secret')
            );
        } catch (\UnexpectedValueException $e){
            $logger -> error($e);
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
            return $response;
        } catch (SignatureVerification $e){
            $logger -> error($e);
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
            return $response;
        }

        if ($event != null){
            //dispatching events by type
            switch ($event -> type){
                case 'customer.subscription.created':
                    return $this -> createCustomerSubscription($event);
                    break;
                case 'customer.subscription.deleted':
                    return $this -> deleteCustomerSubscription($event);
                    break;
                default:
                    $response -> setStatusCode(Response::HTTP_NOT_FOUND);
                    break;
            }
        } else {
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        return $response;
    }

    /**
     * This method give the role of subscriber to an user
     * @param Event $event
     * @return Response
     */
    private function createCustomerSubscription(Event $event)
    {
        $response = new Response();

        //getting subscription
        $subscription = $event -> data -> object;

        //checking customer
        $customerId = $subscription -> customer;

        //retrieving user
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $user = $userRepo -> findOneBy(array(
            'customerId' => $customerId
        ));

        if ($user != null){

            //checking plan
            $plan = $subscription -> plan;

            switch ($plan -> id){
                case 'full-access-monthly':
                    if (!in_array($user::$ROLE_FULL_ACCESS_MONTHLY,$user -> getRoles())){
                        $user -> addRole($user::$ROLE_FULL_ACCESS_MONTHLY);
                        $mongoMan -> persist($user);
                        $mongoMan -> flush();
                    }
                    break;
                case 'full-access-yearly':
                    if (!in_array($user::$ROLE_FULL_ACCESS_YEARLY,$user -> getRoles())){
                        $user -> addRole($user::$ROLE_FULL_ACCESS_YEARLY);
                        $mongoMan -> persist($user);
                        $mongoMan -> flush();
                    }
                    break;
            }

            $response -> setStatusCode(Response::HTTP_OK);
            return $response;
        } else {
            $response -> setStatusCode(Response::HTTP_NOT_FOUND);
        }

        return $response;
    }

    /**
     * This method removes the role associated to a subscription
     * @param Event $event
     * @return Response
     */
    private function deleteCustomerSubscription(Event $event)
    {
        $response = new Response();

        //getting subscription
        $subscription = $event -> data -> object;

        //checking customer
        $customer = $subscription -> customer;

        //retrieving user
        $mongoMan = $this -> get('doctrine_mongodb') -> getManager();
        $userRepo = $mongoMan -> getRepository('BeverageMPBundle:User');
        $user = $userRepo -> findOneBy(array(
            'customerId' => $customer
        ));

        if ($user != null){
            //reseting role
            $user -> resetRoles();

            $mongoMan -> persist($user);
            $mongoMan -> flush();

            $response -> setStatusCode(Response::HTTP_OK);
            $response -> setContent('DONE');
            return $response;
        } else {
            $response -> setStatusCode(Response::HTTP_NOT_FOUND);
        }
        return $response;
    }
}