<?php

namespace BeverageMPApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BeverageMPApiBundle:Default:index.html.twig');
    }
}
