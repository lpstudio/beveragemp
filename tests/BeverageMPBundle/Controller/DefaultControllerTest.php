<?php

namespace BeverageMPBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * Testing index rendering
     */
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/');

        $this->assertContains('The Beverage Marketplace', $client->getResponse()->getContent());
    }

    /**
     * Testing register rendering
     */
    public function testRegister()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/register');

        $this->assertContains('Register', $client->getResponse()->getContent());
    }

    /**
     * Testing connect renderding
     */
    public function testConnect()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/connect');

        $this->assertContains('Connect', $client->getResponse()->getContent());
    }

    /**
     * Testing recover password rendering
     */
    public function testRecoverPassword()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/en/recoverpassword');

        $this->assertContains('Recover password', $client->getResponse()->getContent());
    }
}
