<?php

namespace BeverageMPBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Test class for User controller
 * @package BeverageMPBundle\Tests\Controller
 */
class UserControllerTest extends WebTestCase
{
    /**
     * Test the register method with empty fields.
     */
    public function testRegisterEmptyFields()
    {
        $client = static::createClient();
        $crawler = $client -> request('POST', '/en/register', array(
            'email' => 'test@outlook.com'
        ));
        $this -> assertTrue($client -> getResponse() -> isRedirect('/en/register?error=1&type=1'));
    }

    /**
     * Test the register method with form filled properly
     */
    /*public function testRegisterSuccess()
    {
        $client = static::createClient();
        $crawler = $client -> request('POST','/en/register', array(
            'email' => 'john@doe.com',
            'firstname' => 'John',
            'lastname' => 'Doe',
            'address' => '11 rue de la République 75000 Paris',
            'phone' => '+33601020304',
            'password' => 'azerty2017',
            'passwordconfirm' => 'azerty2017',
            'structure' => 'corporation',
            'businessname' => 'Google Inc.'
        ));
        $this -> assertTrue($client -> getResponse() -> isRedirect('/en/register?success=1'));
    }*/
}