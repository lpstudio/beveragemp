$(function () {
    var $cookieModal = $('#cookieModal'),
        $acceptCookies = $('#acceptCookies');
    if (Cookies.get('accept_cookies') != 1){
        $cookieModal.modal();
    }
    $acceptCookies.click(function () {
        Cookies.set('accept_cookies',1,{expires: 365});
    });
});